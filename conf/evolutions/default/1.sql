# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table fixture (
  fid                           bigint not null,
  date                          varchar(255),
  time                          varchar(255),
  home_score_point              integer,
  home_team                     varchar(255),
  home_score_goal               integer,
  home_score                    integer,
  away_team                     varchar(255),
  away_score_point              integer,
  away_score_goal               integer,
  away_score                    integer,
  home_yellows                  integer,
  home_blacks                   integer,
  home_reds                     integer,
  away_yellows                  integer,
  away_blacks                   integer,
  away_reds                     integer,
  constraint pk_fixture primary key (fid)
);
create sequence fixture_seq;

create table league (
  lid                           bigint not null,
  name                          varchar(255),
  start_date                    varchar(255),
  finish_date                   varchar(255),
  sponsor                       varchar(255),
  constraint pk_league primary key (lid)
);
create sequence league_seq;

create table player (
  pid                           bigint not null,
  num                           integer,
  fname                         varchar(255),
  lname                         varchar(255),
  pos                           varchar(255),
  club                          varchar(255),
  county                        varchar(255),
  age                           integer,
  apps                          integer,
  points                        integer,
  yellows                       integer,
  blacks                        integer,
  reds                          integer,
  constraint pk_player primary key (pid)
);
create sequence player_seq;

create table referee (
  rid                           bigint not null,
  name                          varchar(255),
  county                        varchar(255),
  constraint pk_referee primary key (rid)
);
create sequence referee_seq;

create table team (
  tid                           bigint not null,
  name                          varchar(255),
  prov                          varchar(255),
  manager                       varchar(255),
  stadium                       varchar(255),
  games_played                  integer,
  games_won                     integer,
  games_drew                    integer,
  games_lost                    integer,
  points_for                    integer,
  points_against                integer,
  points_difference             integer,
  league_points                 integer,
  yellows                       integer,
  blacks                        integer,
  reds                          integer,
  constraint pk_team primary key (tid)
);
create sequence team_seq;

create table user (
  email                         varchar(255) not null,
  role                          varchar(255),
  name                          varchar(255),
  password                      varchar(255),
  constraint pk_user primary key (email)
);


# --- !Downs

drop table if exists fixture;
drop sequence if exists fixture_seq;

drop table if exists league;
drop sequence if exists league_seq;

drop table if exists player;
drop sequence if exists player_seq;

drop table if exists referee;
drop sequence if exists referee_seq;

drop table if exists team;
drop sequence if exists team_seq;

drop table if exists user;

