package controllers;

import controllers.security.AuthAdmin;
import controllers.security.Secured;

import play.mvc.*;
import play.data.*;
import play.db.ebean.Transactional;
import views.html.*;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

import views.html.*;

import models.*;
import models.users.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    private FormFactory formFactory;
	
    @Inject
    public HomeController(FormFactory f) {
	this.formFactory = f;
    }	

    private User getUserFromSession() {
        return User.getUserById(session().get("email"));
    }


    public Result index() {
     //List<Player> dirtyPlayer = Player.dirtyPlayer();
     List<Fixture> smallResults = Fixture.smallResults();
     List<Fixture> smallFixture = Fixture.smallFixture();
     List<Team> highestScorers = Team.highestScorers();
     List<Team> teamList = Team.leagueTable();
	return ok(index.render(teamList,highestScorers,smallFixture,smallResults,getUserFromSession()));
    }
	
    public Result live() {
        List<Player> oldestPlayer = Player.oldestPlayer();
        List<Player> topScorer = Player.topScorer();
        List<Player> youngestPlayer = Player.youngestPlayer();
        List<Team> worstDefence = Team.worstDefence();
        List<Team> bestDefence = Team.bestDefence();
        List<Team> highestScorers = Team.highestScorers();
        List<Player> dirtyPlayer = Player.dirtyPlayer();
        List<Team> dirtiestTeam = Team.dirtiestTeam();
	    return ok(live.render(oldestPlayer, topScorer, youngestPlayer, worstDefence, bestDefence, highestScorers, dirtyPlayer, dirtiestTeam, getUserFromSession()));
    }

    public Result fixtures() {
     List<League> roundHeaders = League.findAll();
     List<Fixture> allFixtures = Fixture.allFixtures();
	return ok(fixtures.render(allFixtures,roundHeaders,getUserFromSession()));
    }

    public Result results() {     
     List<League> roundHeaders = League.findAll();
     List<Fixture> ascFixtures = Fixture.ascFixtures();
	return ok(results.render(ascFixtures,roundHeaders,getUserFromSession()));
    }

    public Result table() {
        List<Team> leagueTable = Team.leagueTable();
        return ok(table.render(leagueTable,getUserFromSession()));
    }

}
