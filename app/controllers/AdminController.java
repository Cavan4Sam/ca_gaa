package controllers;

import controllers.security.AuthAdmin;
import controllers.security.Secured;
import play.mvc.*;
import play.data.*;
import play.db.ebean.Transactional;
import views.html.*;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

import views.html.admin.*;
import models.*;
import models.users.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */

@Security.Authenticated(Secured.class)
@With(AuthAdmin.class)
public class AdminController extends Controller {

    private FormFactory formFactory;
	
    @Inject
    public AdminController(FormFactory f) {
		this.formFactory = f;
    }	

    private User getUserFromSession() {
        return User.getUserById(session().get("email"));
    }

	public Result adminIndex() {
		return ok(adminIndex.render(getUserFromSession()));
	}

    public Result fixture() {
		List <Fixture> fixtureList = Fixture.findAll();
		return ok(fixture.render(fixtureList, getUserFromSession()));
    }

	public Result league() {
		List <League> leagueList = League.findAll();
		return ok(league.render(leagueList, getUserFromSession()));
	}

	public Result referee() {
		List <Referee> refereeList = Referee.findAll();
		return ok(referee.render(refereeList, getUserFromSession()));
	}

    public Result player(){
		List<Player> playerList = Player.findAll();
		return ok(player.render(playerList, getUserFromSession()));
	}

	public Result team() {
		List<Team> teamsList = Team.findAll();
		return ok(team.render(teamsList, getUserFromSession()));
    }

    public Result addPlayer() {
		Form<Player> addPlayerForm = formFactory.form(Player.class);
		return ok(addPlayer.render(addPlayerForm, getUserFromSession()));
    }

	public Result addTeam() {
		Form<Team> addTeamForm = formFactory.form(Team.class);
		return ok(addTeam.render(addTeamForm, getUserFromSession()));
	}

	public Result addFixture() {
		Form<Fixture> addFixtureForm = formFactory.form(Fixture.class);
		return ok(addFixture.render(addFixtureForm, getUserFromSession()));
	}

	

    public Result addPlayerSubmit() {
	Form<Player> newPlayerForm = formFactory.form(Player.class).bindFromRequest();
	if(newPlayerForm.hasErrors()) {
		return badRequest(addPlayer.render(newPlayerForm, getUserFromSession()));
	}
	
	Player p = newPlayerForm.get();	

	if (p.getPid() == null) {
		p.save();
	}
	else if (p.getPid() != null) {
		p.update();
	}
		
	flash("success", "Player " + p.getFname() + " " + p.getLname() + " has been created");	
	return redirect(routes.AdminController.player());
    }
    

	public Result addTeamSubmit() {
	Form<Team> newTeamForm = formFactory.form(Team.class).bindFromRequest();
	if(newTeamForm.hasErrors()){
		return badRequest(addTeam.render(newTeamForm, getUserFromSession()));
	}

	Team t = newTeamForm.get();

	if (t.getTid() == null){
		t.save();
	} else if (t.getTid() != null) {
		t.update();
	}

	flash("success", "Team " +t.getName() + " has been created");
	return redirect(routes.AdminController.team());
	}

	public Result addFixtureSubmit() {
	Form<Fixture> newFixtureForm = formFactory.form(Fixture.class).bindFromRequest();
	if(newFixtureForm.hasErrors()){
		return badRequest(addFixture.render(newFixtureForm, getUserFromSession()));
	}

	Fixture f = newFixtureForm.get();

	if (f.getFid() == null){
		f.save();
	} else if (f.getFid() != null) {
		f.update();
	}

	flash("success", "Fixture has been created");
	return redirect(routes.AdminController.fixture());
	}

    public Result deletePlayer(Long pid) {
	Player.find.byId(pid).delete();
	flash("success", "Product has been deleted");
	return redirect(routes.AdminController.player());
    }

	public Result deleteTeam(Long tid) {
	Team.find.byId(tid).delete();
	flash("success", "Team has been deleted");
	return redirect(routes.AdminController.team());
	}

	public Result deleteFixture(Long fid) {
	Fixture.find.byId(fid).delete();
	flash("success", "Fixture has been deleted");
	return redirect(routes.AdminController.fixture());
	}

	@Transactional
    public Result updatePlayer(Long pid) {
	Player p;
	Form<Player> playerForm;
	try {
		p = Player.find.byId(pid);
		playerForm = formFactory.form(Player.class).fill(p);
	} catch (Exception ex) {
		return badRequest("error");
	}
	return ok(addPlayer.render(playerForm, getUserFromSession()));
}

	@Transactional
	public Result updateTeam(Long tid) {
	Team t;
	Form<Team> teamForm;
	try {
		t = Team.find.byId(tid);
		teamForm = formFactory.form(Team.class).fill(t);

	} catch (Exception ex) {
		return badRequest("error");
	}
	return ok(addTeam.render(teamForm, getUserFromSession()));
}

	@Transactional
	public Result updateFixture(Long fid) {
	Fixture f;
	Form<Fixture> fixtureForm;
	try {
		f = Fixture.find.byId(fid);
		fixtureForm = formFactory.form(Fixture.class).fill(f);
	}	catch (Exception ex) {
		return badRequest("error");
	}
	return ok(addFixture.render(fixtureForm, getUserFromSession()));
	}

	public Result controlGame() {
		List<Fixture> RossCavan = Fixture.RossCavan();
		return ok(controlGame.render(RossCavan, getUserFromSession()));
	}
	
	@Transactional
	public Result incHomeGoal() {
		Fixture.homeGoalTally();
	    
		return redirect(routes.AdminController.controlGame());
	}

	public Result addReferee() {
		Form<Referee> addRefereeForm = formFactory.form(Referee.class);
		return ok(addReferee.render(addRefereeForm, getUserFromSession()));
	}

	public Result addRefereeSubmit() {
	Form<Referee> newRefereeForm = formFactory.form(Referee.class).bindFromRequest();
	if(newRefereeForm.hasErrors()){
		return badRequest(addReferee.render(newRefereeForm, getUserFromSession()));
	}

	Referee t = newRefereeForm.get();
		
		t.save();

	flash("success", "Referee " +t.getName() + " has been created");
	return redirect(routes.AdminController.referee());
	}

	public Result deleteReferee(Long rid) {
	Referee.find.byId(rid).delete();
	flash("success", "Team has been deleted");
	return redirect(routes.AdminController.referee());
	}
}
