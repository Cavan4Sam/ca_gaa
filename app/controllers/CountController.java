package controllers;

import javax.inject.*;
import play.*;
import play.mvc.*;

import services.Counter;

import controllers.security.AuthAdmin;
import controllers.security.Secured;
import play.data.*;
import play.db.ebean.Transactional;
import views.html.*;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

import views.html.admin.*;
import models.*;
import models.users.*;


/**
 * This controller demonstrates how to use dependency injection to
 * bind a component into a controller class. The class contains an
 * action that shows an incrementing count to users. The {@link Counter}
 * object is injected by the Guice dependency injection system.
 */
@Singleton
public class CountController extends Controller {

    private final Counter counter;

    @Inject
    public CountController(Counter counter) {
       this.counter = counter;
    }

    private User getUserFromSession() {
        return User.getUserById(session().get("email"));
    }

    /**
     * An action that responds with the {@link Counter}'s current
     * count. The result is plain text. This action is mapped to
     * <code>GET</code> requests with a path of <code>/count</code>
     * requests by an entry in the <code>routes</code> config file.
     */
    public Result count() {
        return ok(Integer.toString(counter.nextCount()));
    }

    // public Result homeTeamScore() { 
    //     Team.setPointsFor(1);
    //     int points = getPointsFor(); 
    //     return ok(adminIndex.render(points,getUserFromSession()));
    //  }

}