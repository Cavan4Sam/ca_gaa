/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.*;
import javax.persistence.*;

import play.data.format.*;
import play.data.validation.*;

import com.avaje.ebean.*;


@Entity
public class League extends Model {
    @Id
    private Long lid;
    @Constraints.Required
    private String name;
    @Constraints.Required
    private String startDate;
    @Constraints.Required
    private String finishDate;
    @Constraints.Required
    private String sponsor;

    public League() {
    }
    
    public League(Long lid, String name, String startDate, String finishDate, String sponsor) {
        this.lid = lid;
        this.name = name;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.sponsor = sponsor;
    }

    public Long getLid() {
        return lid;
    }

    public void setLid(Long lid) {
        this.lid = lid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }
    
    public static Finder<Long,League> find = new Finder<Long,League>(League.class);

    public static List<League> findAll() {
		return League.find.all();
	}
    
    
    
    
}

