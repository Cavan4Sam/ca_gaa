/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.*;
import javax.persistence.*;

import play.data.format.*;
import play.data.validation.*;

import com.avaje.ebean.*;

@Entity
public class Fixture extends Model {
    @Id
    private Long fid;
    @Constraints.Required
    private String date;
    @Constraints.Required
    private String time;
    @Constraints.Required
    private int homeScorePoint;
    @Constraints.Required
    private String homeTeam;
    @Constraints.Required
    private int homeScoreGoal;
    @Constraints.Required
    private int homeScore;
    @Constraints.Required
    private String awayTeam;
    @Constraints.Required
    private int awayScorePoint;
    @Constraints.Required
    private int awayScoreGoal;
    @Constraints.Required
    private int awayScore;
    @Constraints.Required
    private int homeYellows;
    @Constraints.Required
    private int homeBlacks;
    @Constraints.Required
    private int homeReds;
    @Constraints.Required
    private int awayYellows;
    @Constraints.Required
    private int awayBlacks;
    @Constraints.Required
    private int awayReds;

    public static int inc = 1;
    public static int homeGoalTally = 0;
    // private int half;
    // private boolean gameEnd;


    public Fixture() {
    }

    public Fixture(Long fid, String date, String time, String homeTeam, int homeScorePoint, int homeScoreGoal, int homeScore, String awayTeam, int awayScorePoint, int awayScoreGoal, int awayScore, int homeYellows, int homeBlacks, int homeReds, int awayYellows, int awayBlacks, int awayReds) {
        this.fid = fid;
        this.date = date;
        this.time = time;
        this.homeTeam = homeTeam;
        this.homeScorePoint = homeScorePoint;
        this.homeScoreGoal = homeScoreGoal;
        this.homeScore = homeScore;
        this.awayTeam = awayTeam;
        this.awayScorePoint = awayScorePoint;
        this.awayScoreGoal = awayScoreGoal;
        this.awayScore = awayScore;
        this.homeYellows = homeYellows;
        this.homeBlacks = homeBlacks;
        this.homeReds = homeReds;
        this.awayYellows = awayYellows;
        this.awayBlacks = awayBlacks;
        this.awayReds = awayReds;
        // half =  1;
        // gameEnd = false;
    }

    public Long getFid() {
        return fid;
    }

    public void setFid(Long fid) {
        this.fid = fid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public int getHomeScorePoint() {
        return homeScorePoint;
    }

    public void setHomeScorePoint(int homeScorePoint) {
        this.homeScorePoint += homeScorePoint;
    }

    public int getHomeScoreGoal() {
        return homeScoreGoal;
    }

    public void setHomeScoreGoal(int homeScoreGoal) {
        this.homeScoreGoal = homeScoreGoal;
    }

    public static int homeGoalTally() {
       return homeGoalTally = homeGoalTally + inc;
    }
    

    public int getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(int homeScore) {
        this.homeScore += homeScore;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public int getAwayScorePoint() {
        return awayScorePoint;
    }

    public void setAwayScorePoint(int awayScorePoint) {
        this.awayScorePoint += awayScorePoint;
    }

    public int getAwayScoreGoal() {
        return awayScoreGoal;
    }

    public void setAwayScoreGoal(int awayScoreGoal) {
        this.awayScoreGoal += awayScoreGoal;
    }

    public int getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(int awayScore) {
        this.awayScore += awayScore;
    }

    public int getHomeYellows() {
        return homeYellows;
    }

    public void setHomeYellows(int homeYellows) {
        this.homeYellows += homeYellows;
    }

    public int getHomeBlacks() {
        return homeBlacks;
    }

    public void setHomeBlacks(int homeBlacks) {
        this.homeBlacks += homeBlacks;
    }

    public int getHomeReds() {
        return homeReds;
    }

    public void setHomeReds(int homeReds) {
        this.homeReds += homeReds;
    }

    public int getAwayYellows() {
        return awayYellows;
    }

    public void setAwayYellows(int awayYellows) {
        this.awayYellows += awayYellows;
    }

    public int getAwayBlacks() {
        return awayBlacks;
    }

    public void setAwayBlacks(int awayBlacks) {
        this.awayBlacks += awayBlacks;
    }

    public int getAwayReds() {
        return awayReds;
    }

    public void setAwayReds(int awayReds) {
    this.awayReds += awayReds;
    }

    public void incHomeScorePoint(int homeScore) {
        this.homeScore += homeScore;
    }
	public void decHomeScorePoint(int homeScore) {
        this.homeScore -= homeScore;
    }
	
	public void incHomeScoreGoal() {
        setHomeScoreGoal(homeGoalTally());
    }

	public void decHomeScoreGoal(int homeScoreGoal) {
        this.homeScoreGoal -= homeScoreGoal;
    }
	
	
	public void incAwayScorePoint(int awayScorePoint) {
        this.awayScorePoint += awayScorePoint;
    }
	public void decAwayScorePoint(int awayScorePoint) {
        this.awayScorePoint -= awayScorePoint;
    }
	
	public void incAwayScoreGoal(int awayScoreGoal) {
        this.awayScoreGoal += awayScoreGoal;
    }
	public void decAwayScoreGoal(int awayScoreGoal) {
        this.awayScoreGoal -= awayScoreGoal;
    }
	
	
	public void incHomeYellows(int homeYellows) {
        this.homeYellows += homeYellows;
    }
	public void decHomeYellows(int homeYellows) {
        this.homeYellows -= homeYellows;
    }
	
	public void incHomeBlacks(int homeBlacks) {
        this.homeBlacks += homeBlacks;
    }
	public void decHomeBlacks(int homeBlacks) {
        this.homeBlacks -= homeBlacks;
    }
	
	public void incHomeReds(int homeReds) {
        this.homeReds += homeReds;
    }
	public void decHomeReds(int homeReds) {
        this.homeReds -= homeReds;
    }
	
	public void incAwayYellows(int awayYellows) {
        this.awayYellows += awayYellows;
    }
	public void decAwayYellows(int awayYellows) {
        this.awayYellows -= awayYellows;
    }
	
	public void incAwayBlacks(int awayBlacks) {
        this.awayBlacks += awayBlacks;
    }
	public void decAwayBlacks(int awayBlacks) {
        this.homeBlacks -= homeBlacks;
    }
	
	public void incAwayReds(int homeReds) {
        this.awayReds += awayReds;
    }
	public void decAwayReds(int homeReds) {
        this.awayReds -= awayReds;
    }

    // public int getHalf() {
    //     return half;
    // }

    // public void setHalf(int half) {
    //     this.half = half;
    // }

    // public boolean isGameEnd() {
    //     return gameEnd;
    // }

    // public void setGameEnd(boolean gameEnd) {
    //     this.gameEnd = gameEnd;
    // }

    public static Finder<Long,Fixture> find = new Finder<Long,Fixture>(Fixture.class);

    public static List<Fixture> findAll() {
		return Fixture.find.all();
	}

    public static List<Fixture> fixtures() {
        return Fixture.find.where()
        .orderBy("fid asc")
        .findList();
    }

    public static List<Fixture> allFixtures() {
        return Fixture.find.where()
        .orderBy("fid desc")
        .findList();
    }

    public static List<Fixture> ascFixtures() {
        return Fixture.find.where()
        .orderBy("fid asc")
        .findList();
    }

    public static List<Fixture> smallFixture() {
        return Fixture.find.where()
        .orderBy("fid desc")
        .setMaxRows(4)
        .findList();
    }

    public static List<Fixture> smallResults() {
        return Fixture.find.where()
        .orderBy("fid desc")
        .setMaxRows(4)
        .findList();
    }

    public static List<Fixture> RossCavan(){
        return Fixture.find.where()
        .orderBy("fid desc")
        .setMaxRows(1)
        .findList();
    }
    
}
