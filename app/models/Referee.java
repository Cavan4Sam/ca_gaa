/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.*;
import javax.persistence.*;

import play.data.format.*;
import play.data.validation.*;

import com.avaje.ebean.*;

@Entity
public class Referee extends Model {
    @Id
    private Long rid;
    @Constraints.Required
    private String name;
    @Constraints.Required
    private String county;
    
    //@OneToOne
    //private Fixture fixture;

    public Referee(Long rid, String name, String county) {
        this.rid = rid;
        this.name = name;
        this.county = county;
    }

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }
    
    public static Finder<Long,Referee> find = new Finder<Long,Referee>(Referee.class);

    public static List<Referee> findAll() {
		return Referee.find.all();
	}
    
    public static List<Referee> nextRound() {
        return Referee.find.where()
        .orderBy("rid asc")
        .setMaxRows(4)
        .findList();
    }

}

