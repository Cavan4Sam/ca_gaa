/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.*;
import javax.persistence.*;

import play.data.format.*;
import play.data.validation.*;

import com.avaje.ebean.*;

@Entity

public class Team extends Model {
    @Id
    private Long tid;
    @Constraints.Required
    private String name;
    @Constraints.Required
    private String prov;
    @Constraints.Required
    private String manager;
    @Constraints.Required
    private String stadium;
    @Constraints.Required
    private int gamesPlayed;
    @Constraints.Required
    private int gamesWon;
    @Constraints.Required
    private int gamesDrew;
    @Constraints.Required
    private int gamesLost;
    @Constraints.Required
    private int pointsFor;
    @Constraints.Required
    private int pointsAgainst;
    @Constraints.Required
    private int pointsDifference;
    @Constraints.Required
    private int leaguePoints;
    @Constraints.Required
    private int yellows;
    @Constraints.Required
    private int blacks;
    @Constraints.Required
    private int reds;

    public Team() {

    }

    public Team(Long tid, String name, String prov, String manager, String stadium, int gamesPlayed, int gamesWon, int gamesDrew, int gamesLost, int pointsFor, int pointsAgainst, int pointsDifference, int leaguePoints, int yellows, int blacks, int reds) {
        this.tid = tid;
        this.name = name;
        this.prov = prov;
        this.manager = manager;
        this.stadium = stadium;
        this.gamesPlayed = gamesPlayed;
        this.gamesWon = gamesWon;
        this.gamesDrew = gamesDrew;
        this.gamesLost = gamesLost;
        this.pointsFor = pointsFor;
        this.pointsAgainst = pointsAgainst;
        this.leaguePoints = leaguePoints;
        this.yellows = yellows;
        this.blacks = blacks;
        this.reds = reds;
        this.pointsDifference = pointsDifference;
    }

        public Long getTid() {
            return tid;
        }
    
    public void setTid(Long tid) {
        this.tid = tid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public int getGamesWon() {
        return gamesWon;
    }

    public void setGamesWon(int gamesWon) {
        this.gamesWon = gamesWon;
    }

    public int getGamesDrew() {
        return gamesDrew;
    }

    public void setGamesDrew(int gamesDrew) {
        this.gamesDrew = gamesDrew;
    }

    public int getGamesLost() {
        return gamesLost;
    }

    public void setGamesLost(int gamesLost) {
        this.gamesLost = gamesLost;
    }

    public int getPointsFor() {
        return pointsFor;
    }

    public void setPointsFor(int pointsFor) {
        this.pointsFor += pointsFor;
    }

    public int getPointsAgainst() {
        return pointsAgainst;
    }

    public void setPointsAgainst(int pointsAgainst) {
        this.pointsAgainst = pointsAgainst;
    }

    public int getLeaguePoints() {
        return leaguePoints;
    }

    public void setLeaguePoints(int leaguePoints) {
        this.leaguePoints = leaguePoints;
    }

    public int getYellows() {
        return yellows;
    }

    public void setYellows(int yellows) {
        this.yellows = yellows;
    }

    public int getBlacks() {
        return blacks;
    }

    public void setBlacks(int blacks) {
        this.blacks = blacks;
    }

    public int getReds() {
        return reds;
    }

    public void setReds(int reds) {
        this.reds = reds;
    }


    public int getPointsDifference() {
        return pointsDifference;
    }

    public void setPointsDifference(int pointsDifference) {
        this.pointsDifference = pointsDifference;
    }



    public static Finder<Long,Team> find = new Finder<Long,Team>(Team.class);

    public static List<Team> findAll() {
		return Team.find.where().orderBy("name asc").findList();

	}
	
    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<>();
        
        for(Team t: Team.findAll()){
            options.put(t.tid.toString(),t.name);
        }
        return options;
    }

    public static List<Team> leagueTable() {
        return Team.find.where()
        .orderBy("league_points desc, points_difference desc")
        .findList();
    }


    public static List<Team>highestScorers() {
        return Team.find.where()
        .orderBy("points_for desc")
        .setMaxRows(3)
        .findList();
    }

    public static List<Team>worstDefence() {
        return Team.find.where()
        .orderBy("points_against desc")
        .setMaxRows(3)
        .findList();
    }

    public static List<Team>bestDefence() {
        return Team.find.where()
        .orderBy("points_against asc")
        .setMaxRows(3)
        .findList();
    }

    public static List<Team>dirtiestTeam() {
        return Team.find.where()
        .orderBy("yellows desc, reds desc, blacks desc")
        .setMaxRows(6)
        .findList();
    }
 
}
