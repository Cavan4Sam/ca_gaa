
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object main_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object main_Scope1 {
import controllers.security.LoginController
import controllers.security

class main extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[String,models.users.User,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(title: String, user: models.users.User)(content:Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.56*/("""

"""),format.raw/*5.1*/("""<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>"""),_display_(/*16.13*/title),format.raw/*16.18*/("""</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" media="screen" href=""""),_display_(/*19.50*/routes/*19.56*/.Assets.versioned("stylesheets/bootstrap.min.css")),format.raw/*19.106*/("""" >
    <link rel="stylesheet" media="screen" href=""""),_display_(/*20.50*/routes/*20.56*/.Assets.versioned("stylesheets/bootstrap.css")),format.raw/*20.102*/("""">


    <!-- Custom CSS -->
    <link  rel="stylesheet" media="screen" href=""""),_display_(/*24.51*/routes/*24.57*/.Assets.versioned("stylesheets/modern-business.css")),format.raw/*24.109*/("""">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
 		<a class="navbar-brand" href=""""),_display_(/*49.35*/routes/*49.41*/.HomeController.index()),format.raw/*49.64*/("""">GAA Stats Center</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
		            	<a href=""""),_display_(/*55.26*/routes/*55.32*/.HomeController.live()),format.raw/*55.54*/("""">Stats</a>
                    </li>
                    <li>
		            	<a href=""""),_display_(/*58.26*/routes/*58.32*/.HomeController.fixtures()),format.raw/*58.58*/("""">Fixtures</a>
                    </li>
                    <li>
		            	<a href=""""),_display_(/*61.26*/routes/*61.32*/.HomeController.results()),format.raw/*61.57*/("""">Results</a>
                    </li>
                    <li>
		            	<a href=""""),_display_(/*64.26*/routes/*64.32*/.HomeController.table()),format.raw/*64.55*/("""">Tables</a>
                    </li>
                             
                    """),_display_(/*67.22*/if(user != null)/*67.38*/{_display_(Seq[Any](format.raw/*67.39*/("""
                
                        """),format.raw/*69.25*/("""<li>
                              <a href=""""),_display_(/*70.41*/controllers/*70.52*/.security.routes.LoginController.logout()),format.raw/*70.93*/(""""> Logout """),_display_(/*70.104*/user/*70.108*/.getName()),format.raw/*70.118*/("""</a>
                        </li>
                        """)))}/*72.27*/else/*72.32*/{_display_(Seq[Any](format.raw/*72.33*/("""
                        """),format.raw/*73.25*/("""<li>
                           <a href=""""),_display_(/*74.38*/controllers/*74.49*/.security.routes.LoginController.login()),format.raw/*74.89*/(""""> Login</a>
                           </li>
                        """)))}),format.raw/*76.26*/("""    
                        
                """),format.raw/*78.17*/("""</ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	"""),_display_(/*84.3*/content),format.raw/*84.10*/("""
     
        """),format.raw/*86.9*/("""<!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-4">
                </div>

                 <div class="col-lg-4">
                    <p>Copyright&copy GAA Stats Center 2017</p>
                </div>

            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script type="text/javascript" src='"""),_display_(/*103.42*/routes/*103.48*/.Assets.versioned("javascripts/jquery.js")),format.raw/*103.90*/("""'></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src='"""),_display_(/*106.42*/routes/*106.48*/.Assets.versioned("javascripts/bootstrap.min.js")),format.raw/*106.97*/("""'></script>
</body>

</html>
"""))
      }
    }
  }

  def render(title:String,user:models.users.User,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title,user)(content)

  def f:((String,models.users.User) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title,user) => (content) => apply(title,user)(content)

  def ref: this.type = this

}


}
}

/**/
object main extends main_Scope0.main_Scope1.main
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/main.scala.html
                  HASH: 634f6e0d0883da8aad4968c0cf6971320d5305f9
                  MATRIX: 865->75|1014->129|1042->131|1359->421|1385->426|1503->517|1518->523|1590->573|1670->626|1685->632|1753->678|1859->757|1874->763|1948->815|3039->1879|3054->1885|3098->1908|3441->2224|3456->2230|3499->2252|3614->2340|3629->2346|3676->2372|3794->2463|3809->2469|3855->2494|3972->2584|3987->2590|4031->2613|4148->2703|4173->2719|4212->2720|4282->2762|4354->2807|4374->2818|4436->2859|4475->2870|4489->2874|4521->2884|4600->2945|4613->2950|4652->2951|4705->2976|4774->3018|4794->3029|4855->3069|4957->3140|5031->3186|5178->3307|5206->3314|5248->3329|5666->3719|5682->3725|5746->3767|5867->3860|5883->3866|5954->3915
                  LINES: 31->3|36->3|38->5|49->16|49->16|52->19|52->19|52->19|53->20|53->20|53->20|57->24|57->24|57->24|82->49|82->49|82->49|88->55|88->55|88->55|91->58|91->58|91->58|94->61|94->61|94->61|97->64|97->64|97->64|100->67|100->67|100->67|102->69|103->70|103->70|103->70|103->70|103->70|103->70|105->72|105->72|105->72|106->73|107->74|107->74|107->74|109->76|111->78|117->84|117->84|119->86|136->103|136->103|136->103|139->106|139->106|139->106
                  -- GENERATED --
              */
          