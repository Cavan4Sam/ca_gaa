
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object index_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class index extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[List[models.Team],List[models.Team],List[models.Fixture],List[models.Fixture],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(team: List[models.Team], highestScorers: List[models.Team], smallFixture: List[models.Fixture], smallResults: List[models.Fixture], user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.159*/("""
	
"""),_display_(/*3.2*/main("Home", user)/*3.20*/ {_display_(Seq[Any](format.raw/*3.22*/("""	
    """),format.raw/*4.5*/("""<!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
        <div class="row">
			<hr>

            <div class="col-lg-9">
				<img src=""""),_display_(/*12.16*/routes/*12.22*/.Assets.versioned("images/bethereall.jpg ")),format.raw/*12.65*/("""" draggable="false" height="100px" width="400px">
				<!-- image from Google Images -->
			</div>

			<div class="col-lg-3">
				<img src=""""),_display_(/*17.16*/routes/*17.22*/.Assets.versioned("images/allianzFoot.jpg ")),format.raw/*17.66*/("""" draggable="false" height="125px" width="255px">
				<!-- image from Google Images -->
			</div>
         </div>  

		 <hr>
		 <div class="row">

		<div class ="col-md-9">
		<a href="http://www.the42.ie/kerry-dublin-report-3295155-Mar2017/">	<img src=""""),_display_(/*26.82*/routes/*26.88*/.Assets.versioned("images/champs.jpg ")),format.raw/*26.127*/("""" draggable="false" height="400px" width ="750px">
				<!-- image from Google Images -->
			<h4> Dublin still on course for league success, as their unbeaten run continues.</h4> </a>
			<hr>
		</div>

		<div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-check"><a href=""""),_display_(/*35.68*/routes/*35.74*/.HomeController.table()),format.raw/*35.97*/("""">Table</a> (Mini)</h4>
                    </div>
        	

			<table class="table table-bordered table-hover table-condensed">
				<thead>
				<!-- The header row-->
					<tr>
						<th>Team</th>
                        <th>Point Difference</th>
						<th>Points</th>
					</tr>
				</thead>
				<tbody>
					<!-- Product row(s) -->
							<!-- Start of For loop - For each p in products add a row -->
               
				"""),_display_(/*52.6*/for(t <- team) yield /*52.20*/ {_display_(Seq[Any](format.raw/*52.22*/("""
                    
					"""),format.raw/*54.6*/("""<tr>
						<td>"""),_display_(/*55.12*/t/*55.13*/.getName),format.raw/*55.21*/("""</td>
                        <td>"""),_display_(/*56.30*/t/*56.31*/.getPointsDifference),format.raw/*56.51*/("""</td>
						<td>"""),_display_(/*57.12*/t/*57.13*/.getLeaguePoints),format.raw/*57.29*/("""</td>
					</tr>
							""")))}),format.raw/*59.9*/("""
				"""),format.raw/*60.5*/("""</tbody>
			</table>			
		</div>
    </div>
	</div>
        <!-- /.row -->

		<div class ="row">
			<div class="col-md-3">
				<a href="http://www.irishmirror.ie/sport/gaa/gaelic-football/gaelic-football-news/relegation-looms-cavan-defeat-against-10048016">	<img src=""""),_display_(/*69.147*/routes/*69.153*/.Assets.versioned("images/artOne.jpg ")),format.raw/*69.192*/("""" draggable="false" height="200px" width ="250px">
				<!-- image from Google Images -->
				<h5> Cavan on the brink of Relegation.</h5></a>
				<hr>
				</div>

				<div class="col-md-3">
						<a href="http://www.rte.ie/sport/gaa/2017/0413/867500-darren-hughes-ruled-out-for-ulster-championship/">	<img src=""""),_display_(/*76.123*/routes/*76.129*/.Assets.versioned("images/artTwo.jpg ")),format.raw/*76.168*/("""" draggable="false" height="200px" width ="250px">
						<!-- image from Google Images -->
						<h5> Monaghan hit with injuries.</h5> </a>
						<hr>
					</div>
	

 	<div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-check"></i><a href=""""),_display_(/*86.72*/routes/*86.78*/.HomeController.results()),format.raw/*86.103*/("""">Results</a> (Round 6)</h4>
                    </div>
			<table class="table table-bordered table-hover table-condensed">
				<thead>
				<!-- The header row-->
					<tr>
						<th>Date</th>
						<th>Home</th>
						<th>Home Score</th>
						<th>Away</th>
						<th>Away Score</th>

					</tr>
				</thead>
				<tbody>
					<!-- Product row(s) -->
							<!-- Start of For loop - For each p in products add a row -->
               
                
				"""),_display_(/*105.6*/for(r <- smallResults) yield /*105.28*/ {_display_(Seq[Any](format.raw/*105.30*/("""
					"""),format.raw/*106.6*/("""<tr>
						<td>"""),_display_(/*107.12*/r/*107.13*/.getDate),format.raw/*107.21*/("""</td>
						<td>"""),_display_(/*108.12*/r/*108.13*/.getHomeTeam),format.raw/*108.25*/("""</td>
						<td>"""),_display_(/*109.12*/r/*109.13*/.getHomeScoreGoal),format.raw/*109.30*/(""" """),format.raw/*109.31*/("""- """),_display_(/*109.34*/r/*109.35*/.getHomeScorePoint),format.raw/*109.53*/(""" """),format.raw/*109.54*/("""("""),_display_(/*109.56*/r/*109.57*/.getHomeScore),format.raw/*109.70*/(""")</td>
						<td>"""),_display_(/*110.12*/r/*110.13*/.getAwayTeam),format.raw/*110.25*/("""</td>
						<td>"""),_display_(/*111.12*/r/*111.13*/.getAwayScoreGoal),format.raw/*111.30*/(""" """),format.raw/*111.31*/("""- """),_display_(/*111.34*/r/*111.35*/.getAwayScorePoint),format.raw/*111.53*/(""" """),format.raw/*111.54*/("""("""),_display_(/*111.56*/r/*111.57*/.getAwayScore),format.raw/*111.70*/(""")</td>
					</tr>
                            
                            """)))}),format.raw/*114.30*/("""

				"""),format.raw/*116.5*/("""</tbody>
			</table>	
			
                </div>
			<hr>	
        </div>

		</div>

		


		<div class="row">

			<div class="col-md-8">
						<a href="http://www.independent.ie/sport/gaelic-games/gaelic-football/cork-continue-their-winless-start-to-life-in-division-2-as-kildare-maintain-100-record-35444553.html">	<img src=""""),_display_(/*131.191*/routes/*131.197*/.Assets.versioned("images/cork.jpg ")),format.raw/*131.234*/("""" draggable="false" height="250px" width ="600px">
						<!-- image from Google Images -->
						<h5> Corks promotion hopes look bleek.</h5> </a>
				</div>

			 <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-check"><a href=""""),_display_(/*139.68*/routes/*139.74*/.HomeController.fixtures()),format.raw/*139.100*/("""">Fixtures</a> (Round 7)</h4>
                    </div>
                  <table class="table table-bordered table-hover table-condensed">
				<thead>
				<!-- The header row-->
					<tr>
						<th>Date</th>
						<th>Time</th>
						<th>Home</th>
						<th>Away </th>
					</tr>
				</thead>
				<tbody>
					<!-- Product row(s) -->
							<!-- Start of For loop - For each p in products add a row -->
               
				"""),_display_(/*155.6*/for(f <- smallFixture) yield /*155.28*/ {_display_(Seq[Any](format.raw/*155.30*/("""
                    
					"""),format.raw/*157.6*/("""<tr>
						<td>"""),_display_(/*158.12*/f/*158.13*/.getDate),format.raw/*158.21*/("""</td>
						<td>"""),_display_(/*159.12*/f/*159.13*/.getTime),format.raw/*159.21*/("""</td>
						<td>"""),_display_(/*160.12*/f/*160.13*/.getHomeTeam),format.raw/*160.25*/("""</td>
						<td>"""),_display_(/*161.12*/f/*161.13*/.getAwayTeam),format.raw/*161.25*/("""</td>
					</tr>
							""")))}),format.raw/*163.9*/("""
				"""),format.raw/*164.5*/("""</tbody>
			</table>		
                </div>
				
            </div>
			
			</div>
      
<hr>


		</div>
		
""")))}),format.raw/*177.2*/("""
  
   
"""))
      }
    }
  }

  def render(team:List[models.Team],highestScorers:List[models.Team],smallFixture:List[models.Fixture],smallResults:List[models.Fixture],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(team,highestScorers,smallFixture,smallResults,user)

  def f:((List[models.Team],List[models.Team],List[models.Fixture],List[models.Fixture],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (team,highestScorers,smallFixture,smallResults,user) => apply(team,highestScorers,smallFixture,smallResults,user)

  def ref: this.type = this

}


}

/**/
object index extends index_Scope0.index
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 08:46:34 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/index.scala.html
                  HASH: 2fe56b4e8c50646dc4397c6d75ccad82687c8bd8
                  MATRIX: 834->1|1087->158|1116->162|1142->180|1181->182|1213->188|1417->365|1432->371|1496->414|1663->554|1678->560|1743->604|2024->858|2039->864|2100->903|2518->1294|2533->1300|2577->1323|3029->1749|3059->1763|3099->1765|3153->1792|3196->1808|3206->1809|3235->1817|3297->1852|3307->1853|3348->1873|3392->1890|3402->1891|3439->1907|3494->1932|3526->1937|3823->2206|3839->2212|3900->2251|4239->2562|4255->2568|4316->2607|4703->2967|4718->2973|4765->2998|5250->3456|5289->3478|5330->3480|5364->3486|5408->3502|5419->3503|5449->3511|5494->3528|5505->3529|5539->3541|5584->3558|5595->3559|5634->3576|5664->3577|5695->3580|5706->3581|5746->3599|5776->3600|5806->3602|5817->3603|5852->3616|5898->3634|5909->3635|5943->3647|5988->3664|5999->3665|6038->3682|6068->3683|6099->3686|6110->3687|6150->3705|6180->3706|6210->3708|6221->3709|6256->3722|6364->3798|6398->3804|6753->4130|6770->4136|6830->4173|7208->4523|7224->4529|7273->4555|7725->4980|7764->5002|7805->5004|7860->5031|7904->5047|7915->5048|7945->5056|7990->5073|8001->5074|8031->5082|8076->5099|8087->5100|8121->5112|8166->5129|8177->5130|8211->5142|8267->5167|8300->5172|8442->5283
                  LINES: 27->1|32->1|34->3|34->3|34->3|35->4|43->12|43->12|43->12|48->17|48->17|48->17|57->26|57->26|57->26|66->35|66->35|66->35|83->52|83->52|83->52|85->54|86->55|86->55|86->55|87->56|87->56|87->56|88->57|88->57|88->57|90->59|91->60|100->69|100->69|100->69|107->76|107->76|107->76|117->86|117->86|117->86|136->105|136->105|136->105|137->106|138->107|138->107|138->107|139->108|139->108|139->108|140->109|140->109|140->109|140->109|140->109|140->109|140->109|140->109|140->109|140->109|140->109|141->110|141->110|141->110|142->111|142->111|142->111|142->111|142->111|142->111|142->111|142->111|142->111|142->111|142->111|145->114|147->116|162->131|162->131|162->131|170->139|170->139|170->139|186->155|186->155|186->155|188->157|189->158|189->158|189->158|190->159|190->159|190->159|191->160|191->160|191->160|192->161|192->161|192->161|194->163|195->164|208->177
                  -- GENERATED --
              */
          