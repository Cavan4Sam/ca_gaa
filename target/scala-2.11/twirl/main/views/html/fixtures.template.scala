
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object fixtures_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class fixtures extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[List[models.Fixture],List[models.League],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(allFixtures: List[models.Fixture],roundHeaders: List[models.League],user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.95*/("""
"""),_display_(/*2.2*/main("Fixture", user)/*2.23*/ {_display_(Seq[Any](format.raw/*2.25*/("""

"""),format.raw/*4.1*/("""<div class="container">
<div class="col-m-4">
  </div>

<div class="col-m-4">
    <div class="panel panel-default">

<div class="panel-heading">


    """),_display_(/*14.6*/for(h <-roundHeaders) yield /*14.27*/{_display_(Seq[Any](format.raw/*14.28*/("""
      """),format.raw/*15.7*/("""<tr>
          <h3>
          <td>"""),_display_(/*17.16*/h/*17.17*/.getName),format.raw/*17.25*/(""" """),format.raw/*17.26*/("""</td>
          <td>"""),_display_(/*18.16*/h/*18.17*/.getSponsor),format.raw/*18.28*/(""" """),format.raw/*18.29*/("""</td>
          </h3>
      </tr>
    """)))}),format.raw/*21.6*/("""

"""),format.raw/*23.1*/("""</div>

  """),_display_(/*25.4*/for(f <- allFixtures) yield /*25.25*/ {_display_(Seq[Any](format.raw/*25.27*/("""
					"""),format.raw/*26.6*/("""<tr>
             <h5><td>"""),_display_(/*27.23*/f/*27.24*/.getDate),format.raw/*27.32*/("""</td>
           <td>"""),_display_(/*28.17*/f/*28.18*/.getTime),format.raw/*28.26*/("""</td> 
						 <td>"""),_display_(/*29.13*/f/*29.14*/.getHomeTeam),format.raw/*29.26*/("""</td>
           <td> -v- </td> 
					<td>"""),_display_(/*31.11*/f/*31.12*/.getAwayTeam),format.raw/*31.24*/("""</td> </h5>
          </tr>
    """)))}),format.raw/*33.6*/("""

"""),format.raw/*35.1*/("""</div>


</div>

</div>
""")))}))
      }
    }
  }

  def render(allFixtures:List[models.Fixture],roundHeaders:List[models.League],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(allFixtures,roundHeaders,user)

  def f:((List[models.Fixture],List[models.League],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (allFixtures,roundHeaders,user) => apply(allFixtures,roundHeaders,user)

  def ref: this.type = this

}


}

/**/
object fixtures extends fixtures_Scope0.fixtures
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 14:44:16 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/fixtures.scala.html
                  HASH: b34794e4b9e2f8d4aa1012c7402876550f3d707c
                  MATRIX: 803->1|991->94|1018->96|1047->117|1086->119|1114->121|1292->273|1329->294|1368->295|1402->302|1464->337|1474->338|1503->346|1532->347|1580->368|1590->369|1622->380|1651->381|1720->420|1749->422|1786->433|1823->454|1863->456|1896->462|1950->489|1960->490|1989->498|2038->520|2048->521|2077->529|2123->548|2133->549|2166->561|2236->604|2246->605|2279->617|2342->650|2371->652
                  LINES: 27->1|32->1|33->2|33->2|33->2|35->4|45->14|45->14|45->14|46->15|48->17|48->17|48->17|48->17|49->18|49->18|49->18|49->18|52->21|54->23|56->25|56->25|56->25|57->26|58->27|58->27|58->27|59->28|59->28|59->28|60->29|60->29|60->29|62->31|62->31|62->31|64->33|66->35
                  -- GENERATED --
              */
          