
package views.html.admin

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object referee_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class referee extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[models.Referee],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(referee: List[models.Referee], user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.58*/("""

"""),_display_(/*3.2*/admin/*3.7*/.adminMain("Referee", user)/*3.34*/ {_display_(Seq[Any](format.raw/*3.36*/("""
    """),format.raw/*4.5*/("""<!-- Page Content -->
    <div class="container">
        <!-- /.row -->
     <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Referee
                    <small>List of Referees</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href=""""),_display_(/*14.35*/routes/*14.41*/.HomeController.index()),format.raw/*14.64*/("""">Home</a>
                    </li>
                    <li class="active">Referee</li>
                </ol>
            </div>
        </div>
    
        <!-- /.row -->

<div class="row">
	<div class="col-lg-12">
		<table class="table table-bordered table-hover table-condensed">
			<thead>
			<!-- The header row-->
				<tr>
					<th>RefereeID</th>
					<th>Name</th>
					<th>County</th>
				</tr>
			</thead>
			<tbody>
				<!-- Product row(s) -->
						<!-- Start of For loop - For each p in products add a row -->
			"""),_display_(/*37.5*/for(r <- referee) yield /*37.22*/ {_display_(Seq[Any](format.raw/*37.24*/("""
				"""),format.raw/*38.5*/("""<tr>
					<td>"""),_display_(/*39.11*/r/*39.12*/.getRid),format.raw/*39.19*/("""</td>
					<td>"""),_display_(/*40.11*/r/*40.12*/.getName),format.raw/*40.20*/("""</td>
					<td>"""),_display_(/*41.11*/r/*41.12*/.getCounty),format.raw/*41.22*/("""</td>
					<td>
						<a href=""""),_display_(/*43.17*/routes/*43.23*/.AdminController.deleteReferee(r.getRid)),format.raw/*43.63*/("""" class="btn-xs btn-danger"
						onclick="return confirmDel();">
						<span class="glyphicon glyphicon-trash"></span>
						</a>
					</td>
					</tr>
						""")))}),format.raw/*49.8*/(""" 
			"""),format.raw/*50.4*/("""</tbody>
		</table>
		<p>
			<a href=""""),_display_(/*53.14*/routes/*53.20*/.AdminController.addReferee()),format.raw/*53.49*/("""">
			<button class="btn btn-primary">Add a Referee</button></a>
		</p>
	</div>
</div>
	<script>
		function confirmDel() """),format.raw/*59.25*/("""{"""),format.raw/*59.26*/("""
			"""),format.raw/*60.4*/("""return confirm('Are you sure');
		"""),format.raw/*61.3*/("""}"""),format.raw/*61.4*/("""
	"""),format.raw/*62.2*/("""</script>
	


""")))}),format.raw/*66.2*/("""
"""))
      }
    }
  }

  def render(referee:List[models.Referee],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(referee,user)

  def f:((List[models.Referee],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (referee,user) => apply(referee,user)

  def ref: this.type = this

}


}

/**/
object referee extends referee_Scope0.referee
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/admin/referee.scala.html
                  HASH: 9481686595a364b170b8c943e9e01ba1e54b1f2f
                  MATRIX: 787->1|938->57|966->60|978->65|1013->92|1052->94|1083->99|1480->469|1495->475|1539->498|2092->1025|2125->1042|2165->1044|2197->1049|2239->1064|2249->1065|2277->1072|2320->1088|2330->1089|2359->1097|2402->1113|2412->1114|2443->1124|2502->1156|2517->1162|2578->1202|2768->1362|2800->1367|2866->1406|2881->1412|2931->1441|3080->1562|3109->1563|3140->1567|3201->1601|3229->1602|3258->1604|3303->1619
                  LINES: 27->1|32->1|34->3|34->3|34->3|34->3|35->4|45->14|45->14|45->14|68->37|68->37|68->37|69->38|70->39|70->39|70->39|71->40|71->40|71->40|72->41|72->41|72->41|74->43|74->43|74->43|80->49|81->50|84->53|84->53|84->53|90->59|90->59|91->60|92->61|92->61|93->62|97->66
                  -- GENERATED --
              */
          