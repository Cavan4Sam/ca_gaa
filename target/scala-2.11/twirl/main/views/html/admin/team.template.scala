
package views.html.admin

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object team_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class team extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[models.Team],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(team: List[models.Team], user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.52*/("""

"""),_display_(/*3.2*/admin/*3.7*/.adminMain("Team", user)/*3.31*/ {_display_(Seq[Any](format.raw/*3.33*/("""
    """),format.raw/*4.5*/("""<!-- Page Content -->
    <div class="container">
        <!-- /.row -->
     <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Team
                    <small>List of Teams</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href=""""),_display_(/*14.35*/routes/*14.41*/.HomeController.index()),format.raw/*14.64*/("""">Home</a>
                    </li>
                    <li class="active">Team</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

<div class="row">
    <table class="table table-bordered table-hover table-condensed">
        <thead>
          <!-- The header row-->
			<tr>
				<th>TeamID</th>
				<th>County</th>
				<th>Province</th>
				<th>Manager</th>
				<th>Stadium</th>
				<th>Games Played</th>
				<th>Games Won</th>
				<th>Games Drew</th>
				<th>Games Lost</th>
				<th>Points Scored</th>
				<th>Points Conceeded</th>
				<th>League Points</th>
				<th>Total Yellow Cards</th>
				<th>Total Black Cards</th>
				<th>Total Red Cards</th>
			
			</tr>
        </thead>
		<tbody>
            <!-- Product row(s) -->
					<!-- Start of For loop - For each p in products add a row -->
		"""),_display_(/*48.4*/for(t <- team) yield /*48.18*/ {_display_(Seq[Any](format.raw/*48.20*/("""
			"""),format.raw/*49.4*/("""<tr>
				<td>"""),_display_(/*50.10*/t/*50.11*/.getTid),format.raw/*50.18*/("""</td>
				<td>"""),_display_(/*51.10*/t/*51.11*/.getName),format.raw/*51.19*/("""</td>
				<td>"""),_display_(/*52.10*/t/*52.11*/.getProv),format.raw/*52.19*/("""</td>
				<td>"""),_display_(/*53.10*/t/*53.11*/.getManager),format.raw/*53.22*/("""</td>
				<td>"""),_display_(/*54.10*/t/*54.11*/.getStadium),format.raw/*54.22*/("""</td>
				<td>"""),_display_(/*55.10*/t/*55.11*/.getGamesPlayed),format.raw/*55.26*/("""</td>
				<td>"""),_display_(/*56.10*/t/*56.11*/.getGamesWon),format.raw/*56.23*/("""</td>
				<td>"""),_display_(/*57.10*/t/*57.11*/.getGamesDrew),format.raw/*57.24*/("""</td>
				<td>"""),_display_(/*58.10*/t/*58.11*/.getGamesLost),format.raw/*58.24*/("""</td>
				<td>"""),_display_(/*59.10*/t/*59.11*/.getPointsFor),format.raw/*59.24*/("""</td>
				<td>"""),_display_(/*60.10*/t/*60.11*/.getPointsAgainst),format.raw/*60.28*/("""</td>
				<td>"""),_display_(/*61.10*/t/*61.11*/.getLeaguePoints),format.raw/*61.27*/("""</td>
				<td>"""),_display_(/*62.10*/t/*62.11*/.getYellows),format.raw/*62.22*/("""</td>
				<td>"""),_display_(/*63.10*/t/*63.11*/.getBlacks),format.raw/*63.21*/("""</td>
				<td>"""),_display_(/*64.10*/t/*64.11*/.getReds),format.raw/*64.19*/("""</td>

				<td>
					<a href=""""),_display_(/*67.16*/routes/*67.22*/.AdminController.updateTeam(t.getTid)),format.raw/*67.59*/("""" class="btn-xs btn-danger">
					<span class="glyphicon glyphicon-pencil"></span></a>
				</td>

			</tr>
					""")))}),format.raw/*72.7*/(""" 
         """),format.raw/*73.10*/("""</tbody>
    </table>

</div>




""")))}),format.raw/*81.2*/("""
"""))
      }
    }
  }

  def render(team:List[models.Team],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(team,user)

  def f:((List[models.Team],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (team,user) => apply(team,user)

  def ref: this.type = this

}


}

/**/
object team extends team_Scope0.team
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/admin/team.scala.html
                  HASH: 1e5320c35783c395e321951ee60923048ce2ca49
                  MATRIX: 778->1|923->51|951->54|963->59|995->83|1034->85|1065->90|1456->454|1471->460|1515->483|2374->1316|2404->1330|2444->1332|2475->1336|2516->1350|2526->1351|2554->1358|2596->1373|2606->1374|2635->1382|2677->1397|2687->1398|2716->1406|2758->1421|2768->1422|2800->1433|2842->1448|2852->1449|2884->1460|2926->1475|2936->1476|2972->1491|3014->1506|3024->1507|3057->1519|3099->1534|3109->1535|3143->1548|3185->1563|3195->1564|3229->1577|3271->1592|3281->1593|3315->1606|3357->1621|3367->1622|3405->1639|3447->1654|3457->1655|3494->1671|3536->1686|3546->1687|3578->1698|3620->1713|3630->1714|3661->1724|3703->1739|3713->1740|3742->1748|3800->1779|3815->1785|3873->1822|4016->1935|4055->1946|4120->1981
                  LINES: 27->1|32->1|34->3|34->3|34->3|34->3|35->4|45->14|45->14|45->14|79->48|79->48|79->48|80->49|81->50|81->50|81->50|82->51|82->51|82->51|83->52|83->52|83->52|84->53|84->53|84->53|85->54|85->54|85->54|86->55|86->55|86->55|87->56|87->56|87->56|88->57|88->57|88->57|89->58|89->58|89->58|90->59|90->59|90->59|91->60|91->60|91->60|92->61|92->61|92->61|93->62|93->62|93->62|94->63|94->63|94->63|95->64|95->64|95->64|98->67|98->67|98->67|103->72|104->73|112->81
                  -- GENERATED --
              */
          