
package views.html.admin

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object addReferee_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object addReferee_Scope1 {
import helper._

class addReferee extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Form[models.Referee],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(addRefereeForm: Form[models.Referee], user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.65*/("""
"""),_display_(/*3.2*/admin/*3.7*/.adminMain("Add Referee", user)/*3.38*/ {_display_(Seq[Any](format.raw/*3.40*/("""
	"""),format.raw/*4.2*/("""<h3>Add a new Referee</h3>
	
	"""),_display_(/*6.3*/form(action = routes.AdminController.addRefereeSubmit(), 'class -> "form-horizontal", 'role->"form")/*6.103*/ {_display_(Seq[Any](format.raw/*6.105*/("""
		"""),_display_(/*7.4*/inputText(addRefereeForm("tid"), '_label -> "Referee ID", 'hidden -> "hidden")),format.raw/*7.82*/("""
		"""),_display_(/*8.4*/inputText(addRefereeForm("name"), '_label -> "Name", 'class -> "form-control")),format.raw/*8.82*/("""
		"""),_display_(/*9.4*/inputText(addRefereeForm("county"), '_label -> "County", 'class -> "form-control")),format.raw/*9.86*/("""

		"""),_display_(/*11.4*/inputText(addRefereeForm("tid"), '_label -> "RefereeID", 'hidden -> "hidden")),format.raw/*11.81*/("""
		
	"""),format.raw/*13.2*/("""<div class="actions">
		<input type="submit" value="Add Referee" class="btn btn-primary">
			<a href=""""),_display_(/*15.14*/routes/*15.20*/.HomeController.index()),format.raw/*15.43*/(""""
				<button class="btn btn-warning">Cancel</button>
			</a>
	</div>
	""")))}),format.raw/*19.3*/("""
""")))}),format.raw/*20.2*/("""
"""))
      }
    }
  }

  def render(addRefereeForm:Form[models.Referee],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(addRefereeForm,user)

  def f:((Form[models.Referee],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (addRefereeForm,user) => apply(addRefereeForm,user)

  def ref: this.type = this

}


}
}

/**/
object addReferee extends addReferee_Scope0.addReferee_Scope1.addReferee
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/admin/addReferee.scala.html
                  HASH: 98eaf7716aa833a8e08d36ecfbcfed8d038fa32d
                  MATRIX: 842->18|1000->81|1027->83|1039->88|1078->119|1117->121|1145->123|1201->154|1310->254|1350->256|1379->260|1477->338|1506->342|1604->420|1633->424|1735->506|1766->511|1864->588|1896->593|2026->696|2041->702|2085->725|2187->797|2219->799
                  LINES: 30->2|35->2|36->3|36->3|36->3|36->3|37->4|39->6|39->6|39->6|40->7|40->7|41->8|41->8|42->9|42->9|44->11|44->11|46->13|48->15|48->15|48->15|52->19|53->20
                  -- GENERATED --
              */
          