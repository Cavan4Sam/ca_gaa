
package views.html.admin

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object adminMain_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object adminMain_Scope1 {
import controllers.security.LoginController
import controllers.security

class adminMain extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[String,models.users.User,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*3.2*/(title: String, user: models.users.User)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*3.57*/("""

"""),format.raw/*5.1*/("""<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>"""),_display_(/*16.13*/title),format.raw/*16.18*/("""</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" media="screen" href=""""),_display_(/*19.50*/routes/*19.56*/.Assets.versioned("stylesheets/bootstrap.min.css")),format.raw/*19.106*/("""" >
    <link rel="stylesheet" media="screen" href=""""),_display_(/*20.50*/routes/*20.56*/.Assets.versioned("stylesheets/bootstrap.css")),format.raw/*20.102*/("""">


    <!-- Custom CSS -->
    <link  rel="stylesheet" media="screen" href=""""),_display_(/*24.51*/routes/*24.57*/.Assets.versioned("stylesheets/modern-business.css")),format.raw/*24.109*/("""">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
 		<a class="navbar-brand" href=""""),_display_(/*49.35*/routes/*49.41*/.AdminController.adminIndex()),format.raw/*49.70*/("""">GAA Stats Center</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href=""""),_display_(/*55.35*/routes/*55.41*/.AdminController.fixture()),format.raw/*55.67*/("""">Fixtures</a>
                    </li>
                    <li>
                        <a href=""""),_display_(/*58.35*/routes/*58.41*/.AdminController.player()),format.raw/*58.66*/("""">Players</a>
                    </li>
                    <li>
                        <a href=""""),_display_(/*61.35*/routes/*61.41*/.AdminController.team()),format.raw/*61.64*/("""">Teams</a>
                    </li>
                    <li>
            			<a href=""""),_display_(/*64.26*/routes/*64.32*/.AdminController.referee()),format.raw/*64.58*/("""">Referees</a>
                    </li>

                    <li>
                        <a href=""""),_display_(/*68.35*/routes/*68.41*/.AdminController.controlGame()),format.raw/*68.71*/("""">Control Game</a>
                    </li>

                 <li>     
                       """),_display_(/*72.25*/if(user != null)/*72.41*/{_display_(Seq[Any](format.raw/*72.42*/("""
                        """),format.raw/*73.25*/("""<li> 
                              <a href=""""),_display_(/*74.41*/controllers/*74.52*/.security.routes.LoginController.logout()),format.raw/*74.93*/(""""> Logout """),_display_(/*74.104*/user/*74.108*/.getName()),format.raw/*74.118*/("""</a>
                         </li>
                        """)))}/*76.27*/else/*76.32*/{_display_(Seq[Any](format.raw/*76.33*/("""
                       """),format.raw/*77.24*/("""<li>
                              <a href=""""),_display_(/*78.41*/controllers/*78.52*/.security.routes.LoginController.login()),format.raw/*78.92*/(""""> Login</a>
                         </li>
                        """)))}),format.raw/*80.26*/("""
                    
                  """),format.raw/*82.19*/("""</li>                  
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	"""),_display_(/*89.3*/content),format.raw/*89.10*/("""
     
        """),format.raw/*91.9*/("""<!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-4">
                   
                </div>
                    <div class="col-lg-4">
                    <p>Copyright &copy; GAA Stats Center 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script type="text/javascript" src='"""),_display_(/*107.42*/routes/*107.48*/.Assets.versioned("javascripts/jquery.js")),format.raw/*107.90*/("""'></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src='"""),_display_(/*110.42*/routes/*110.48*/.Assets.versioned("javascripts/bootstrap.min.js")),format.raw/*110.97*/("""'></script>
</body>

</html>
"""))
      }
    }
  }

  def render(title:String,user:models.users.User,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title,user)(content)

  def f:((String,models.users.User) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title,user) => (content) => apply(title,user)(content)

  def ref: this.type = this

}


}
}

/**/
object adminMain extends adminMain_Scope0.adminMain_Scope1.adminMain
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/admin/adminMain.scala.html
                  HASH: 0575ef3f48236b4524c4ff33d32b2cfdf10fd73a
                  MATRIX: 886->75|1036->130|1064->132|1381->422|1407->427|1525->518|1540->524|1612->574|1692->627|1707->633|1775->679|1881->758|1896->764|1970->816|3061->1880|3076->1886|3126->1915|3478->2240|3493->2246|3540->2272|3667->2372|3682->2378|3728->2403|3854->2502|3869->2508|3913->2531|4028->2619|4043->2625|4090->2651|4218->2752|4233->2758|4284->2788|4408->2885|4433->2901|4472->2902|4525->2927|4598->2973|4618->2984|4680->3025|4719->3036|4733->3040|4765->3050|4845->3112|4858->3117|4897->3118|4949->3142|5021->3187|5041->3198|5102->3238|5202->3307|5270->3347|5457->3508|5485->3515|5527->3530|5968->3943|5984->3949|6048->3991|6169->4084|6185->4090|6256->4139
                  LINES: 31->3|36->3|38->5|49->16|49->16|52->19|52->19|52->19|53->20|53->20|53->20|57->24|57->24|57->24|82->49|82->49|82->49|88->55|88->55|88->55|91->58|91->58|91->58|94->61|94->61|94->61|97->64|97->64|97->64|101->68|101->68|101->68|105->72|105->72|105->72|106->73|107->74|107->74|107->74|107->74|107->74|107->74|109->76|109->76|109->76|110->77|111->78|111->78|111->78|113->80|115->82|122->89|122->89|124->91|140->107|140->107|140->107|143->110|143->110|143->110
                  -- GENERATED --
              */
          