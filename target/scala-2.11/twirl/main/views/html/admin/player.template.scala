
package views.html.admin

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object player_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class player extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[models.Player],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(player: List[models.Player], user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.56*/("""

"""),_display_(/*3.2*/admin/*3.7*/.adminMain("Player",user)/*3.32*/ {_display_(Seq[Any](format.raw/*3.34*/("""
    """),format.raw/*4.5*/("""<!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->                

            <div class="col-lg-12">
                <h1 class="page-header">Players
                    <small>List</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href=""""),_display_(/*14.35*/routes/*14.41*/.HomeController.index()),format.raw/*14.64*/("""">Home</a>
                    </li>
                    <li class="active">Player</li>
                </ol>
            </div>
        <!-- /.row -->
 

        <div class="row">
		    """),_display_(/*23.8*/if(flash.containsKey("success"))/*23.40*/ {_display_(Seq[Any](format.raw/*23.42*/("""
		    """),format.raw/*24.7*/("""<div class=-"alert alert-success">
		        """),_display_(/*25.12*/flash/*25.17*/.get("success")),format.raw/*25.32*/("""
		    """),format.raw/*26.7*/("""</div>
	       """)))}),format.raw/*27.10*/("""

                    """),format.raw/*29.21*/("""<div class="col-sm-12">
            <table class="table table-bordered table-hover table-condensed">
            <thead>


          <!-- The header row-->
                <tr>
                    <th>Player ID</th>
                    <th>County</th>
                    <th>Number</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Age</th>
                    <th>Position</th>
                    <th>Apperances</th>
                    <th>Club</th>
                    <th>Points</th>
                    <th>Yellows</th>
                    <th>Blacks</th>
                    <th>Reds</th>
                </tr>
          </thead>
          <tbody>
            <!-- Product row(s) -->
					<!-- Start of For loop - For each p in products add a row -->
	        """),_display_(/*54.11*/for(p <- player) yield /*54.27*/ {_display_(Seq[Any](format.raw/*54.29*/("""
	            """),format.raw/*55.14*/("""<tr>
                    <td>"""),_display_(/*56.26*/p/*56.27*/.getPid),format.raw/*56.34*/("""</td>
                    <td>"""),_display_(/*57.26*/p/*57.27*/.getCounty),format.raw/*57.37*/("""</td>
                    <td>"""),_display_(/*58.26*/p/*58.27*/.getNum),format.raw/*58.34*/("""</td>
                    <td>"""),_display_(/*59.26*/p/*59.27*/.getFname),format.raw/*59.36*/("""</td>
                    <td>"""),_display_(/*60.26*/p/*60.27*/.getLname),format.raw/*60.36*/("""</td>
                    <td>"""),_display_(/*61.26*/p/*61.27*/.getAge),format.raw/*61.34*/("""</td>
                    <td>"""),_display_(/*62.26*/p/*62.27*/.getPos),format.raw/*62.34*/("""</td>
                    <td>"""),_display_(/*63.26*/p/*63.27*/.getApps),format.raw/*63.35*/("""</td>
                    <td>"""),_display_(/*64.26*/p/*64.27*/.getClub),format.raw/*64.35*/("""</td>
                    <td>"""),_display_(/*65.26*/p/*65.27*/.getPoints),format.raw/*65.37*/("""</td>
                    <td>"""),_display_(/*66.26*/p/*66.27*/.getYellows),format.raw/*66.38*/("""</td>
                    <td>"""),_display_(/*67.26*/p/*67.27*/.getBlacks),format.raw/*67.37*/("""</td>
                    <td>"""),_display_(/*68.26*/p/*68.27*/.getReds),format.raw/*68.35*/("""</td>

                <td>
					<a href=""""),_display_(/*71.16*/routes/*71.22*/.AdminController.updatePlayer(p.getPid())),format.raw/*71.63*/("""" class="btn-xs btn-danger">
					<span class="glyphicon glyphicon-pencil"></span></a>
				</td>
	</tr>
					""")))}),format.raw/*75.7*/(""" 

          """),format.raw/*77.11*/("""</tbody>
        </table>
</div>
	
""")))}),format.raw/*81.2*/("""
"""))
      }
    }
  }

  def render(player:List[models.Player],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(player,user)

  def f:((List[models.Player],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (player,user) => apply(player,user)

  def ref: this.type = this

}


}

/**/
object player extends player_Scope0.player
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/admin/player.scala.html
                  HASH: 2cf9033e194c9f019e5365b441843f862d392a41
                  MATRIX: 784->1|933->55|961->58|973->63|1006->88|1045->90|1076->95|1433->425|1448->431|1492->454|1706->642|1747->674|1787->676|1821->683|1894->729|1908->734|1944->749|1978->756|2025->772|2075->794|2929->1621|2961->1637|3001->1639|3043->1653|3100->1683|3110->1684|3138->1691|3196->1722|3206->1723|3237->1733|3295->1764|3305->1765|3333->1772|3391->1803|3401->1804|3431->1813|3489->1844|3499->1845|3529->1854|3587->1885|3597->1886|3625->1893|3683->1924|3693->1925|3721->1932|3779->1963|3789->1964|3818->1972|3876->2003|3886->2004|3915->2012|3973->2043|3983->2044|4014->2054|4072->2085|4082->2086|4114->2097|4172->2128|4182->2129|4213->2139|4271->2170|4281->2171|4310->2179|4380->2222|4395->2228|4457->2269|4597->2379|4638->2392|4704->2428
                  LINES: 27->1|32->1|34->3|34->3|34->3|34->3|35->4|45->14|45->14|45->14|54->23|54->23|54->23|55->24|56->25|56->25|56->25|57->26|58->27|60->29|85->54|85->54|85->54|86->55|87->56|87->56|87->56|88->57|88->57|88->57|89->58|89->58|89->58|90->59|90->59|90->59|91->60|91->60|91->60|92->61|92->61|92->61|93->62|93->62|93->62|94->63|94->63|94->63|95->64|95->64|95->64|96->65|96->65|96->65|97->66|97->66|97->66|98->67|98->67|98->67|99->68|99->68|99->68|102->71|102->71|102->71|106->75|108->77|112->81
                  -- GENERATED --
              */
          