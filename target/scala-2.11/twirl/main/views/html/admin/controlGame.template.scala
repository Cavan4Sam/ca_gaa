
package views.html.admin

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object controlGame_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class controlGame extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[models.Fixture],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(RossCavan: List[models.Fixture], user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.60*/("""

"""),_display_(/*3.2*/admin/*3.7*/.adminMain("LiveGame", user)/*3.35*/ {_display_(Seq[Any](format.raw/*3.37*/("""
    """),format.raw/*4.5*/("""<!-- Page Content -->
    <div class="container">
				"""),_display_(/*6.6*/for(ls <- RossCavan) yield /*6.26*/ {_display_(Seq[Any](format.raw/*6.28*/("""
			"""),format.raw/*7.4*/("""<h1>"""),_display_(/*7.9*/ls/*7.11*/.getHomeTeam),format.raw/*7.23*/(""" 	"""),format.raw/*7.25*/("""<a id="clickHomeGoal">0</a> - <a id="clickHomePoint">0</a>  (<a id="totalHome">0</a>) vs """),_display_(/*7.115*/ls/*7.117*/.getAwayTeam),format.raw/*7.129*/(""" 	"""),format.raw/*7.131*/("""<a id="clickAwayGoal">0</a> - <a id="clickAwayPoint">0</a> (<a id="totalAway">0</a>)</h1>

        <div class="LeftPlayerStats">
				

				

        <div class="actions">

			<script type="text/javascript">
				var clickHomeGoal = 0;
				var clickHomePoint = 0;

				var clickAwayGoal = 0;
				var clickAwayPoint = 0;

				var totalHome = 0;
				var totalAway = 0;
				
				var homeGoals = 0;
				var homePoints = 0;

				var awayGoals = 0;
				var awayPoints = 0;

				var homeTally =0;
				var awayTally =0;

				var homeYellow =0;
				var awayYellow =0;

				var homeBlack =0;
				var awayBlack =0;

				var homeRed =0;
				var awayRed =0;


				function incHomeGoal() """),format.raw/*45.28*/("""{"""),format.raw/*45.29*/("""
					"""),format.raw/*46.6*/("""clickHomeGoal +=1;
					totalHome +=3;
					homeTally +=3;
					homeGoals +=1;

					document.getElementById("clickHomeGoal").innerHTML = clickHomeGoal;
					document.getElementById("totalHome").innerHTML = totalHome;
					document.getElementById("homeGoals").innerHTML = homeGoals;
					document.getElementById("homeTally").innerHTML = homeTally;
				"""),format.raw/*55.5*/("""}"""),format.raw/*55.6*/(""";

				function decHomeGoal() """),format.raw/*57.28*/("""{"""),format.raw/*57.29*/("""
					"""),format.raw/*58.6*/("""clickHomeGoal -=1;
					totalHome -=3;
					homeTally -=3;
					homeGoals -=1;

					document.getElementById("clickHomeGoal").innerHTML = clickHomeGoal;
					document.getElementById("totalHome").innerHTML = totalHome;
					document.getElementById("homeGoals").innerHTML = homeGoals;
					document.getElementById("homeTally").innerHTML = homeTally;
				"""),format.raw/*67.5*/("""}"""),format.raw/*67.6*/(""";

				function incHomePoint() """),format.raw/*69.29*/("""{"""),format.raw/*69.30*/("""
					"""),format.raw/*70.6*/("""clickHomePoint +=1;
					totalHome +=1;
					homeTally +=1;
					homePoints +=1;

					document.getElementById("clickHomePoint").innerHTML = clickHomePoint;
					document.getElementById("totalHome").innerHTML = totalHome;
					document.getElementById("homePoints").innerHTML = homePoints;
					document.getElementById("homeTally").innerHTML = homeTally;
					
				"""),format.raw/*80.5*/("""}"""),format.raw/*80.6*/(""";

				function decHomePoint() """),format.raw/*82.29*/("""{"""),format.raw/*82.30*/("""
					"""),format.raw/*83.6*/("""clickHomePoint -=1;
					totalHome -=1;
					homeTally -=1;
					homePoints -=1;

					document.getElementById("clickHomePoint").innerHTML = clickHomePoint;
					document.getElementById("totalHome").innerHTML = totalHome;
					document.getElementById("homePoints").innerHTML = homePoints;
					document.getElementById("homeTally").innerHTML = homeTally;
				"""),format.raw/*92.5*/("""}"""),format.raw/*92.6*/(""";


				function incAwayGoal() """),format.raw/*95.28*/("""{"""),format.raw/*95.29*/("""
					"""),format.raw/*96.6*/("""clickAwayGoal +=1;
					totalAway +=3;
					awayTally +=3;
					awayGoals +=1;

					document.getElementById("clickAwayGoal").innerHTML = clickAwayGoal;
					document.getElementById("totalAway").innerHTML = totalAway;
					document.getElementById("awayGoals").innerHTML = awayGoals;
					document.getElementById("awayTally").innerHTML = awayTally;
				"""),format.raw/*105.5*/("""}"""),format.raw/*105.6*/(""";

				function decAwayGoal() """),format.raw/*107.28*/("""{"""),format.raw/*107.29*/("""
					"""),format.raw/*108.6*/("""clickAwayGoal -=1;
					totalAway -=3;
					awayTally -=3;
					awayGoals -=1;

					document.getElementById("clickAwayGoal").innerHTML = clickAwayGoal;
					document.getElementById("totalAway").innerHTML = totalAway;
					document.getElementById("awayGoals").innerHTML = awayGoals;
					document.getElementById("awayTally").innerHTML = awayTally;
				"""),format.raw/*117.5*/("""}"""),format.raw/*117.6*/(""";

				function incAwayPoint() """),format.raw/*119.29*/("""{"""),format.raw/*119.30*/("""
					"""),format.raw/*120.6*/("""clickAwayPoint +=1;
					totalAway +=1;
					awayTally +=1;
					awayPoints +=1;

					document.getElementById("clickAwayPoint").innerHTML = clickAwayPoint;
					document.getElementById("totalAway").innerHTML = totalAway;
					document.getElementById("awayPoints").innerHTML = awayPoints;
					document.getElementById("awayTally").innerHTML = awayTally;
				"""),format.raw/*129.5*/("""}"""),format.raw/*129.6*/(""";

				function decAwayPoint() """),format.raw/*131.29*/("""{"""),format.raw/*131.30*/("""
					"""),format.raw/*132.6*/("""clickAwayPoint -=1;
					totalAway -=1;
					awayTally -=1;
					awayPoints -=1;

					document.getElementById("clickAwayPoint").innerHTML = clickAwayPoint;
					document.getElementById("totalAway").innerHTML = totalAway;
					document.getElementById("awayPoints").innerHTML = awayPoints;
					document.getElementById("awayTally").innerHTML = awayTally;
				"""),format.raw/*141.5*/("""}"""),format.raw/*141.6*/(""";

				function incHomeYellow() """),format.raw/*143.30*/("""{"""),format.raw/*143.31*/("""
					"""),format.raw/*144.6*/("""homeYellow +=1;
					document.getElementById("homeYellow").innerHTML = homeYellow;
				"""),format.raw/*146.5*/("""}"""),format.raw/*146.6*/("""

				"""),format.raw/*148.5*/("""function decHomeYellow() """),format.raw/*148.30*/("""{"""),format.raw/*148.31*/("""
					"""),format.raw/*149.6*/("""homeYellow -=1;
					document.getElementById("homeYellow").innerHTML = homeYellow;
				"""),format.raw/*151.5*/("""}"""),format.raw/*151.6*/("""

				"""),format.raw/*153.5*/("""function incHomeBlack() """),format.raw/*153.29*/("""{"""),format.raw/*153.30*/("""
					"""),format.raw/*154.6*/("""homeBlack +=1;
					document.getElementById("homeBlack").innerHTML = homeBlack;
				"""),format.raw/*156.5*/("""}"""),format.raw/*156.6*/("""

				"""),format.raw/*158.5*/("""function decHomeBlack() """),format.raw/*158.29*/("""{"""),format.raw/*158.30*/("""
					"""),format.raw/*159.6*/("""homeBlack -=1;
					document.getElementById("homeBlack").innerHTML = homeBlack;
				"""),format.raw/*161.5*/("""}"""),format.raw/*161.6*/("""

				"""),format.raw/*163.5*/("""function incHomeRed() """),format.raw/*163.27*/("""{"""),format.raw/*163.28*/("""
					"""),format.raw/*164.6*/("""homeRed +=1;
					document.getElementById("homeRed").innerHTML = homeRed;
				"""),format.raw/*166.5*/("""}"""),format.raw/*166.6*/("""

				"""),format.raw/*168.5*/("""function decHomeRed() """),format.raw/*168.27*/("""{"""),format.raw/*168.28*/("""
					"""),format.raw/*169.6*/("""homeRed -=1;
					document.getElementById("homeRed").innerHTML = homeRed;
				"""),format.raw/*171.5*/("""}"""),format.raw/*171.6*/("""





	"""),format.raw/*177.2*/("""function incAwayYellow() """),format.raw/*177.27*/("""{"""),format.raw/*177.28*/("""
					"""),format.raw/*178.6*/("""awayYellow +=1;
					document.getElementById("awayYellow").innerHTML = awayYellow;
				"""),format.raw/*180.5*/("""}"""),format.raw/*180.6*/("""

				"""),format.raw/*182.5*/("""function decAwayYellow() """),format.raw/*182.30*/("""{"""),format.raw/*182.31*/("""
					"""),format.raw/*183.6*/("""awayYellow -=1;
					document.getElementById("awayYellow").innerHTML = awayYellow;
				"""),format.raw/*185.5*/("""}"""),format.raw/*185.6*/("""

				"""),format.raw/*187.5*/("""function incAwayBlack() """),format.raw/*187.29*/("""{"""),format.raw/*187.30*/("""
					"""),format.raw/*188.6*/("""awayBlack +=1;
					document.getElementById("awayBlack").innerHTML = awayBlack;
				"""),format.raw/*190.5*/("""}"""),format.raw/*190.6*/("""

				"""),format.raw/*192.5*/("""function decAwayBlack() """),format.raw/*192.29*/("""{"""),format.raw/*192.30*/("""
					"""),format.raw/*193.6*/("""awayBlack -=1;
					document.getElementById("awayBlack").innerHTML = awayBlack;
				"""),format.raw/*195.5*/("""}"""),format.raw/*195.6*/("""

				"""),format.raw/*197.5*/("""function incAwayRed() """),format.raw/*197.27*/("""{"""),format.raw/*197.28*/("""
					"""),format.raw/*198.6*/("""awayRed +=1;
					document.getElementById("awayRed").innerHTML = awayRed;
				"""),format.raw/*200.5*/("""}"""),format.raw/*200.6*/("""

				"""),format.raw/*202.5*/("""function decAwayRed() """),format.raw/*202.27*/("""{"""),format.raw/*202.28*/("""
					"""),format.raw/*203.6*/("""awayRed -=1;
					document.getElementById("awayRed").innerHTML = awayRed;
				"""),format.raw/*205.5*/("""}"""),format.raw/*205.6*/("""





			"""),format.raw/*211.4*/("""</script>
					<button type="button" onclick="incHomeGoal()" class="btn btn-primary">+</button>
					<button type="button" onclick="decHomeGoal()" class="btn btn-primary">-</button>

    		</div>

        <div class="actions">
		    	<button type="button" onclick="incHomePoint()" class="btn btn-primary">+</button>
				<button type="button" onclick="decHomePoint()" class="btn btn-primary">-</button>
    	</div>

 <div class="actions">
		    	<input type="submit" value="+" class="btn btn-primary">
					<button class="btn btn-warning">-</button>
					</a>
    		</div>

        <div class="actions">
		    	<button type="button" onclick="incHomeYellow()" class="btn btn-primary">+</button>
				<button type="button" onclick="decHomeYellow()" class="btn btn-primary">-</button>
    	</div>


        <div class="actions">
		    	<button type="button" onclick="incHomeBlack()" class="btn btn-primary">+</button>
				<button type="button" onclick="decHomeBlack()" class="btn btn-primary">-</button>
    	</div>

        <div class="actions">
		    	<button type="button" onclick="incHomeRed()" class="btn btn-primary">+</button>
				<button type="button" onclick="decHomeRed()" class="btn btn-primary">-</button>
    	</div>

		
				 </div>
		<div class="centre">
			<h3><a id="homeGoals">0</a>		Goals		<a id="awayGoals">0</a>	</h3>
			<h3><a id="homePoints">0</a>	Points		<a id="awayPoints">0</a></h3>
			<h3><a id="homeTally">0</a>		Total Points		<a id="awayTally">0</a>	</h3>
			<h3><a id="homeYellow">0</a>			Yellow Cards			<a id="awayYellow">0</a></h3>
			<h3><a id="homeBlack">0</a>			Black Cards			<a id="awayBlack">0</a></h3>
			<h3><a id="homeRed">0</a>		Red Cards			<a id="awayRed">0</a></h3>
		</div>

     <div class="RightPlayerStats">
			 <h3>  </h3>


        <div class="actions">
					<button type="button" onclick="incAwayGoal()" class="btn btn-primary">+</button>
					<button type="button" onclick="decAwayGoal()" class="btn btn-primary">-</button>
    		</div>

			<div class="actions">
		    	<button type="button" onclick="incAwayPoint()" class="btn btn-primary">+</button>
				<button type="button" onclick="decAwayPoint()" class="btn btn-primary">-</button>
    		</div>




    
        <div class="actions">
		    	<input type="submit" value="+" class="btn btn-primary">
					<button class="btn btn-warning">-</button>
					</a>
    		</div>

	        <div class="actions">
		    	<button type="button" onclick="incAwayYellow()" class="btn btn-primary">+</button>
				<button type="button" onclick="decAwayYellow()" class="btn btn-primary">-</button>
    	</div>


        <div class="actions">
		    	<button type="button" onclick="incAwayBlack()" class="btn btn-primary">+</button>
				<button type="button" onclick="decAwayBlack()" class="btn btn-primary">-</button>
    	</div>

        <div class="actions">
		    	<button type="button" onclick="incAwayRed()" class="btn btn-primary">+</button>
				<button type="button" onclick="decAwayRed()" class="btn btn-primary">-</button>
    	</div>
    </div>
					""")))}),format.raw/*295.7*/("""



"""),format.raw/*299.1*/("""</div>
""")))}),format.raw/*300.2*/("""
"""))
      }
    }
  }

  def render(RossCavan:List[models.Fixture],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(RossCavan,user)

  def f:((List[models.Fixture],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (RossCavan,user) => apply(RossCavan,user)

  def ref: this.type = this

}


}

/**/
object controlGame extends controlGame_Scope0.controlGame
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/admin/controlGame.scala.html
                  HASH: 104fce50f0d108336b1d35b9fc44026a21600433
                  MATRIX: 795->1|948->59|976->62|988->67|1024->95|1063->97|1094->102|1174->157|1209->177|1248->179|1278->183|1308->188|1318->190|1350->202|1379->204|1496->294|1507->296|1540->308|1570->310|2270->982|2299->983|2332->989|2711->1341|2739->1342|2797->1372|2826->1373|2859->1379|3238->1731|3266->1732|3325->1763|3354->1764|3387->1770|3778->2134|3806->2135|3865->2166|3894->2167|3927->2173|4312->2531|4340->2532|4399->2563|4428->2564|4461->2570|4841->2922|4870->2923|4929->2953|4959->2954|4993->2960|5373->3312|5402->3313|5462->3344|5492->3345|5526->3351|5912->3709|5941->3710|6001->3741|6031->3742|6065->3748|6451->4106|6480->4107|6541->4139|6571->4140|6605->4146|6720->4233|6749->4234|6783->4240|6837->4265|6867->4266|6901->4272|7016->4359|7045->4360|7079->4366|7132->4390|7162->4391|7196->4397|7308->4481|7337->4482|7371->4488|7424->4512|7454->4513|7488->4519|7600->4603|7629->4604|7663->4610|7714->4632|7744->4633|7778->4639|7884->4717|7913->4718|7947->4724|7998->4746|8028->4747|8062->4753|8168->4831|8197->4832|8232->4839|8286->4864|8316->4865|8350->4871|8465->4958|8494->4959|8528->4965|8582->4990|8612->4991|8646->4997|8761->5084|8790->5085|8824->5091|8877->5115|8907->5116|8941->5122|9053->5206|9082->5207|9116->5213|9169->5237|9199->5238|9233->5244|9345->5328|9374->5329|9408->5335|9459->5357|9489->5358|9523->5364|9629->5442|9658->5443|9692->5449|9743->5471|9773->5472|9807->5478|9913->5556|9942->5557|9979->5566|13050->8606|13082->8610|13121->8618
                  LINES: 27->1|32->1|34->3|34->3|34->3|34->3|35->4|37->6|37->6|37->6|38->7|38->7|38->7|38->7|38->7|38->7|38->7|38->7|38->7|76->45|76->45|77->46|86->55|86->55|88->57|88->57|89->58|98->67|98->67|100->69|100->69|101->70|111->80|111->80|113->82|113->82|114->83|123->92|123->92|126->95|126->95|127->96|136->105|136->105|138->107|138->107|139->108|148->117|148->117|150->119|150->119|151->120|160->129|160->129|162->131|162->131|163->132|172->141|172->141|174->143|174->143|175->144|177->146|177->146|179->148|179->148|179->148|180->149|182->151|182->151|184->153|184->153|184->153|185->154|187->156|187->156|189->158|189->158|189->158|190->159|192->161|192->161|194->163|194->163|194->163|195->164|197->166|197->166|199->168|199->168|199->168|200->169|202->171|202->171|208->177|208->177|208->177|209->178|211->180|211->180|213->182|213->182|213->182|214->183|216->185|216->185|218->187|218->187|218->187|219->188|221->190|221->190|223->192|223->192|223->192|224->193|226->195|226->195|228->197|228->197|228->197|229->198|231->200|231->200|233->202|233->202|233->202|234->203|236->205|236->205|242->211|326->295|330->299|331->300
                  -- GENERATED --
              */
          