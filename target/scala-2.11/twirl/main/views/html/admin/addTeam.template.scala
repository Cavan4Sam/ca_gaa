
package views.html.admin

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object addTeam_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object addTeam_Scope1 {
import helper._

class addTeam extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Form[models.Team],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(addTeamForm: Form[models.Team], user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.59*/("""
"""),_display_(/*3.2*/admin/*3.7*/.adminMain("Add Team", user)/*3.35*/ {_display_(Seq[Any](format.raw/*3.37*/("""
	"""),format.raw/*4.2*/("""<h3>Add a new Team</h3>
	
	"""),_display_(/*6.3*/form(action = routes.AdminController.addTeamSubmit(), 'class -> "form-horizontal", 'role->"form")/*6.100*/ {_display_(Seq[Any](format.raw/*6.102*/("""
		"""),_display_(/*7.4*/inputText(addTeamForm("tid"), '_label -> "Team ID", 'hidden -> "hidden")),format.raw/*7.76*/("""
		"""),_display_(/*8.4*/inputText(addTeamForm("name"), '_label -> "Name", 'class -> "form-control")),format.raw/*8.79*/("""
		"""),_display_(/*9.4*/inputText(addTeamForm("prov"), '_label -> "Province", 'class -> "form-control")),format.raw/*9.83*/("""
		"""),_display_(/*10.4*/inputText(addTeamForm("manager"), '_label -> "Manager", 'class -> "form-control")),format.raw/*10.85*/("""
		"""),_display_(/*11.4*/inputText(addTeamForm("stadium"), '_label -> "Stadium", 'class -> "form-control")),format.raw/*11.85*/("""
		"""),_display_(/*12.4*/inputText(addTeamForm("games_played"), '_label -> "Games Played", 'class -> "form-control")),format.raw/*12.95*/("""
		"""),_display_(/*13.4*/inputText(addTeamForm("games_won"), '_label -> "Games Won", 'class -> "form-control")),format.raw/*13.89*/("""
		"""),_display_(/*14.4*/inputText(addTeamForm("games_drew"), '_label -> "Games Drawn", 'class -> "form-control")),format.raw/*14.92*/("""
		"""),_display_(/*15.4*/inputText(addTeamForm("games_lost"), '_label -> "Games Lost", 'class -> "form-control")),format.raw/*15.91*/("""
		"""),_display_(/*16.4*/inputText(addTeamForm("points_for"), '_label -> "Points For", 'class -> "form-control")),format.raw/*16.91*/("""
		"""),_display_(/*17.4*/inputText(addTeamForm("points_against"), '_label -> "Points Against", 'class -> "form-control")),format.raw/*17.99*/("""
		"""),_display_(/*18.4*/inputText(addTeamForm("league_points"), '_label -> "League Points", 'class -> "form-control")),format.raw/*18.97*/("""
		"""),_display_(/*19.4*/inputText(addTeamForm("yellows"), '_label -> "Yellow Cards", 'class -> "form-control")),format.raw/*19.90*/("""
		"""),_display_(/*20.4*/inputText(addTeamForm("blacks"), '_label -> "Black Cards", 'class -> "form-control")),format.raw/*20.88*/("""
		"""),_display_(/*21.4*/inputText(addTeamForm("reds"), '_label -> "Red Cards", 'class -> "form-control")),format.raw/*21.84*/("""

		
	"""),format.raw/*24.2*/("""<div class="actions">
		<input type="submit" value="Add Team" class="btn btn-primary">
			<a href=""""),_display_(/*26.14*/routes/*26.20*/.HomeController.index()),format.raw/*26.43*/(""""
				<button class="btn btn-warning">Cancel</button>
			</a>
	</div>
	""")))}),format.raw/*30.3*/("""
""")))}),format.raw/*31.2*/("""
"""))
      }
    }
  }

  def render(addTeamForm:Form[models.Team],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(addTeamForm,user)

  def f:((Form[models.Team],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (addTeamForm,user) => apply(addTeamForm,user)

  def ref: this.type = this

}


}
}

/**/
object addTeam extends addTeam_Scope0.addTeam_Scope1.addTeam
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/admin/addTeam.scala.html
                  HASH: ebe28c47f5fa0c8a50a4c689fa53c09a9efe306d
                  MATRIX: 830->18|982->75|1009->77|1021->82|1057->110|1096->112|1124->114|1177->142|1283->239|1323->241|1352->245|1444->317|1473->321|1568->396|1597->400|1696->479|1726->483|1828->564|1858->568|1960->649|1990->653|2102->744|2132->748|2238->833|2268->837|2377->925|2407->929|2515->1016|2545->1020|2653->1107|2683->1111|2799->1206|2829->1210|2943->1303|2973->1307|3080->1393|3110->1397|3215->1481|3245->1485|3346->1565|3379->1571|3506->1671|3521->1677|3565->1700|3667->1772|3699->1774
                  LINES: 30->2|35->2|36->3|36->3|36->3|36->3|37->4|39->6|39->6|39->6|40->7|40->7|41->8|41->8|42->9|42->9|43->10|43->10|44->11|44->11|45->12|45->12|46->13|46->13|47->14|47->14|48->15|48->15|49->16|49->16|50->17|50->17|51->18|51->18|52->19|52->19|53->20|53->20|54->21|54->21|57->24|59->26|59->26|59->26|63->30|64->31
                  -- GENERATED --
              */
          