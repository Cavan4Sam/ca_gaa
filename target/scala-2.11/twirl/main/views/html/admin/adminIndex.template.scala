
package views.html.admin

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object adminIndex_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class adminIndex extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.27*/("""

"""),_display_(/*3.2*/admin/*3.7*/.adminMain("adminIndex", user)/*3.37*/ {_display_(Seq[Any](format.raw/*3.39*/("""
    """),format.raw/*4.5*/("""<!-- Page Content -->
    <div class="container">
        <!-- /.row -->
     <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">

            
                <div class="col-lg-3">
                        
                                <div class="Image">
                                   <a href=""""),_display_(/*15.46*/routes/*15.52*/.AdminController.fixture()),format.raw/*15.78*/(""""> <img src=""""),_display_(/*15.92*/routes/*15.98*/.Assets.versioned("images/game.jpg ")),format.raw/*15.135*/("""" draggable="false" height="150px" width="248px">
                                </div>

                                    <div class="Description">
                                       <h3> Fixtures</h3> </a>
                                    </div>
            
                        

                    </div>
                       
                   <div class="col-lg-3">
                    

                                <div class="Image">
                                    <a href=""""),_display_(/*30.47*/routes/*30.53*/.AdminController.referee()),format.raw/*30.79*/(""""> <img src=""""),_display_(/*30.93*/routes/*30.99*/.Assets.versioned("images/ref.jpg ")),format.raw/*30.135*/("""" draggable="false" height="150px" width="248px">
                                </div>

                                    <div class="Description">
                                       <h3> Referees</h3> </a>
                                    </div>

                    
                    </div>

                    <div class="col-lg-3">
                        

                                <div class="Image">
                                 <a href=""""),_display_(/*44.44*/routes/*44.50*/.AdminController.player()),format.raw/*44.75*/(""""> <img src=""""),_display_(/*44.89*/routes/*44.95*/.Assets.versioned("images/dubplayer.jpg ")),format.raw/*44.137*/("""" draggable="false" height="150px" width="248px">
                                </div>

                                    <div class="Description">
                                       <h3> Player</h3> </a>
                                    </div>
                    </div>
                
                    <div class="col-lg-3">

                                <div class="Image">
                                   <a href=""""),_display_(/*55.46*/routes/*55.52*/.AdminController.team()),format.raw/*55.75*/(""""> <img src=""""),_display_(/*55.89*/routes/*55.95*/.Assets.versioned("images/team.jpg ")),format.raw/*55.132*/("""" draggable="false" height="150px" width="248px">
                                </div>

                                    <div class="Description">
                                       <h3> Team</h3> </a>
                                    </div>
                    </div>
                
	<!--<div class="actions">
		<input type="submit" value="Home Point" class="btn btn-primary">
		
				<button class="btn btn-warning">Cancel</button>
			</a>
	</div>

    <div>
            <h1> """),_display_(/*71.19*/team),format.raw/*71.23*/(""" """),format.raw/*71.24*/("""</h1>
     </div>-->
            </div>
        </div>
""")))}))
      }
    }
  }

  def render(user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(user)

  def f:((models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (user) => apply(user)

  def ref: this.type = this

}


}

/**/
object adminIndex extends adminIndex_Scope0.adminIndex
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/admin/adminIndex.scala.html
                  HASH: a21eedce1fed50c49ad710914e2ca620f8ac7d87
                  MATRIX: 772->1|892->26|920->29|932->34|970->64|1009->66|1040->71|1416->420|1431->426|1478->452|1519->466|1534->472|1593->509|2130->1019|2145->1025|2192->1051|2233->1065|2248->1071|2306->1107|2805->1579|2820->1585|2866->1610|2907->1624|2922->1630|2986->1672|3454->2113|3469->2119|3513->2142|3554->2156|3569->2162|3628->2199|4147->2691|4172->2695|4201->2696
                  LINES: 27->1|32->1|34->3|34->3|34->3|34->3|35->4|46->15|46->15|46->15|46->15|46->15|46->15|61->30|61->30|61->30|61->30|61->30|61->30|75->44|75->44|75->44|75->44|75->44|75->44|86->55|86->55|86->55|86->55|86->55|86->55|102->71|102->71|102->71
                  -- GENERATED --
              */
          