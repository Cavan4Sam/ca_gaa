
package views.html.admin

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object addFixture_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object addFixture_Scope1 {
import helper._

class addFixture extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Form[models.Fixture],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(addFixtureForm: Form[models.Fixture], user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.65*/("""
"""),_display_(/*3.2*/admin/*3.7*/.adminMain("Add Fixture", user)/*3.38*/ {_display_(Seq[Any](format.raw/*3.40*/("""
	"""),format.raw/*4.2*/("""<h3>Add a new Fixture</h3>
	
	"""),_display_(/*6.3*/form(action = routes.AdminController.addFixtureSubmit(), 'class -> "form-horizontal", 'role->"form")/*6.103*/{_display_(Seq[Any](format.raw/*6.104*/("""
		"""),_display_(/*7.4*/inputText(addFixtureForm("fid"), '_label -> "FixtureID", 'hidden -> "hidden")),format.raw/*7.81*/("""
		"""),_display_(/*8.4*/inputText(addFixtureForm("date"), '_label -> "Date", 'class -> "form-control")),format.raw/*8.82*/("""
		"""),_display_(/*9.4*/inputText(addFixtureForm("time"), '_label -> "Time", 'class -> "form-control")),format.raw/*9.82*/("""		
		"""),_display_(/*10.4*/inputText(addFixtureForm("home_score_point"), '_label -> "Home Point", 'class -> "form-control")),format.raw/*10.100*/("""
		"""),_display_(/*11.4*/inputText(addFixtureForm("home_team"), '_label -> "Home Team", 'class -> "form-control")),format.raw/*11.92*/("""
		"""),_display_(/*12.4*/inputText(addFixtureForm("home_score_goal"), '_label -> "Home Goal", 'class -> "form-control")),format.raw/*12.98*/("""
		"""),_display_(/*13.4*/inputText(addFixtureForm("home_score"), '_label -> "Home Team Score", 'class -> "form-control")),format.raw/*13.99*/("""
		"""),_display_(/*14.4*/inputText(addFixtureForm("away_team"), '_label -> "Away Team", 'class -> "form-control")),format.raw/*14.92*/("""
		"""),_display_(/*15.4*/inputText(addFixtureForm("away_score_point"), '_label -> "Away Point", 'class -> "form-control")),format.raw/*15.100*/("""
		"""),_display_(/*16.4*/inputText(addFixtureForm("away_score_goal"), '_label -> "Away Goal", 'class -> "form-contol")),format.raw/*16.97*/("""
		"""),_display_(/*17.4*/inputText(addFixtureForm("away_team_score"), '_label -> "Away Team Score", 'class -> "form-control")),format.raw/*17.104*/("""
		"""),_display_(/*18.4*/inputText(addFixtureForm("home_yellows"), '_label -> "Home Yellow", 'class -> "form-control")),format.raw/*18.97*/("""
		"""),_display_(/*19.4*/inputText(addFixtureForm("home_blacks"), '_label -> "Home Black", 'class -> "form-control")),format.raw/*19.95*/("""
		"""),_display_(/*20.4*/inputText(addFixtureForm("home_reds"), '_label -> "Home Red", 'class -> "form-control")),format.raw/*20.91*/("""
		"""),_display_(/*21.4*/inputText(addFixtureForm("away_yellows"), '_label -> "Away Yellow", 'class -> "form-control")),format.raw/*21.97*/("""
		"""),_display_(/*22.4*/inputText(addFixtureForm("away_blacks"), '_label -> "Away Black", 'class -> "form-control")),format.raw/*22.95*/("""
		"""),_display_(/*23.4*/inputText(addFixtureForm("away_reds"), '_label -> "Away Red", 'class -> "form-control")),format.raw/*23.91*/("""

		
	"""),format.raw/*26.2*/("""<div class="actions">
		<input type="submit" value="Add Fixture" class="btn btn-primary">
			<a href=""""),_display_(/*28.14*/routes/*28.20*/.AdminController.fixture()),format.raw/*28.46*/(""""
				<button class="btn btn-warning">Cancel</button>
			</a>
	</div>
	""")))}),format.raw/*32.3*/("""
""")))}))
      }
    }
  }

  def render(addFixtureForm:Form[models.Fixture],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(addFixtureForm,user)

  def f:((Form[models.Fixture],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (addFixtureForm,user) => apply(addFixtureForm,user)

  def ref: this.type = this

}


}
}

/**/
object addFixture extends addFixture_Scope0.addFixture_Scope1.addFixture
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/admin/addFixture.scala.html
                  HASH: c3e8ea696fd028e3712e4d56488d0b73108e1e75
                  MATRIX: 842->18|1000->81|1027->83|1039->88|1078->119|1117->121|1145->123|1201->154|1310->254|1349->255|1378->259|1475->336|1504->340|1602->418|1631->422|1729->500|1761->506|1879->602|1909->606|2018->694|2048->698|2163->792|2193->796|2309->891|2339->895|2448->983|2478->987|2596->1083|2626->1087|2740->1180|2770->1184|2892->1284|2922->1288|3036->1381|3066->1385|3178->1476|3208->1480|3316->1567|3346->1571|3460->1664|3490->1668|3602->1759|3632->1763|3740->1850|3773->1856|3903->1959|3918->1965|3965->1991|4067->2063
                  LINES: 30->2|35->2|36->3|36->3|36->3|36->3|37->4|39->6|39->6|39->6|40->7|40->7|41->8|41->8|42->9|42->9|43->10|43->10|44->11|44->11|45->12|45->12|46->13|46->13|47->14|47->14|48->15|48->15|49->16|49->16|50->17|50->17|51->18|51->18|52->19|52->19|53->20|53->20|54->21|54->21|55->22|55->22|56->23|56->23|59->26|61->28|61->28|61->28|65->32
                  -- GENERATED --
              */
          