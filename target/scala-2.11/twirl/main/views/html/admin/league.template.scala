
package views.html.admin

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object league_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class league extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[models.League],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(league: List[models.League], user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.56*/("""

"""),_display_(/*3.2*/admin/*3.7*/.adminMain("League", user)/*3.33*/ {_display_(Seq[Any](format.raw/*3.35*/("""
    """),format.raw/*4.5*/("""<!-- Page Content -->
    <div class="container">
        <!-- /.row -->
     <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">League
                    <small>List of Leagues</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href=""""),_display_(/*14.35*/routes/*14.41*/.HomeController.index()),format.raw/*14.64*/("""">Home</a>
                    </li>
                    <li class="active">League</li>
                </ol>
            </div>
        </div>
    </div>
        <!-- /.row -->

<div class="row">
	<div class="col-lg-12">
		<table class="table table-bordered table-hover table-condensed">
		<thead>
			<!-- The header row-->
				<tr>
					<th>LeagueID</th>
					<th>League Name</th>
					<th>Start Date</th>
					<th>Finish Date</th>				
				</tr>
			</thead>
			<tbody>
				<!-- Product row(s) -->
						<!-- Start of For loop - For each p in products add a row -->
			"""),_display_(/*38.5*/for(l <- league) yield /*38.21*/ {_display_(Seq[Any](format.raw/*38.23*/("""
				"""),format.raw/*39.5*/("""<tr>
					<td>"""),_display_(/*40.11*/l/*40.12*/.getLid),format.raw/*40.19*/("""</td>
					<td>"""),_display_(/*41.11*/l/*41.12*/.getName),format.raw/*41.20*/("""</td>
					<td>"""),_display_(/*42.11*/l/*42.12*/.getStartDate),format.raw/*42.25*/("""</td>
					<td>"""),_display_(/*43.11*/l/*43.12*/.getFinishDate),format.raw/*43.26*/("""</td>
					<td>"""),_display_(/*44.11*/l/*44.12*/.getSponsor),format.raw/*44.23*/("""</td>
				</tr>
						""")))}),format.raw/*46.8*/(""" 
			"""),format.raw/*47.4*/("""</tbody>
		</table>
	</div>
</div>


""")))}),format.raw/*53.2*/("""
"""))
      }
    }
  }

  def render(league:List[models.League],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(league,user)

  def f:((List[models.League],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (league,user) => apply(league,user)

  def ref: this.type = this

}


}

/**/
object league extends league_Scope0.league
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/admin/league.scala.html
                  HASH: dee2efae183d51762f43017832f8fd9fa64bf618
                  MATRIX: 784->1|933->55|961->58|973->63|1007->89|1046->91|1077->96|1472->464|1487->470|1531->493|2128->1064|2160->1080|2200->1082|2232->1087|2274->1102|2284->1103|2312->1110|2355->1126|2365->1127|2394->1135|2437->1151|2447->1152|2481->1165|2524->1181|2534->1182|2569->1196|2612->1212|2622->1213|2654->1224|2707->1247|2739->1252|2807->1290
                  LINES: 27->1|32->1|34->3|34->3|34->3|34->3|35->4|45->14|45->14|45->14|69->38|69->38|69->38|70->39|71->40|71->40|71->40|72->41|72->41|72->41|73->42|73->42|73->42|74->43|74->43|74->43|75->44|75->44|75->44|77->46|78->47|84->53
                  -- GENERATED --
              */
          