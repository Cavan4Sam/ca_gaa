
package views.html.admin

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object user_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class user extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.27*/("""

"""),_display_(/*3.2*/main("User", user)/*3.20*/  {_display_(Seq[Any](format.raw/*3.23*/("""

"""),format.raw/*5.1*/("""<section>
  <div class="single-page main-grid-border">
    <div class="container">

      <div class="row">
        <div class="col-sm-10">
          <div class="well">
            <h4>User Details</h4>
            	"""),_display_(/*13.15*/if(flash.containsKey("error"))/*13.45*/ {_display_(Seq[Any](format.raw/*13.47*/("""
				"""),format.raw/*14.5*/("""<div class="alert alert-warning">
				"""),_display_(/*15.6*/flash/*15.11*/.get("loginRequired")),format.raw/*15.32*/("""
				"""),format.raw/*16.5*/("""</div>
			""")))}),format.raw/*17.5*/("""
          """),format.raw/*18.11*/("""</div>
        </div>
        <div class="col-sm-10">
          <table class="table table-bordered table-hover table-condensed">
            <thead>
              <!-- The header row-->
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Password</th>
        
              </tr>
            </thead>
                <tbody>

            <tr>

            
                <td>"""),_display_(/*36.22*/user/*36.26*/.getName),format.raw/*36.34*/("""</td>
                <td>"""),_display_(/*37.22*/user/*37.26*/.getEmail),format.raw/*37.35*/("""</td>
                <td>"""),_display_(/*38.22*/user/*38.26*/.getPassword),format.raw/*38.38*/("""</td>

        
       <td>
                  				
                  <a href= class="btn-xs btn-danger">
                    <span class="glyphicon glyphicon-pencil"></span>
                  </a>


                </td>
         
               
                
            </tbody>
 
          </table>


        </div>



      </div>

    </div>
  </div>


  <script> 
  function confirmDelete() """),format.raw/*68.28*/("""{"""),format.raw/*68.29*/("""
    """),format.raw/*69.5*/("""return confirm('Are you sure?');
  """),format.raw/*70.3*/("""}"""),format.raw/*70.4*/("""
  """),format.raw/*71.3*/("""</script>
</section>



""")))}))
      }
    }
  }

  def render(user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(user)

  def f:((models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (user) => apply(user)

  def ref: this.type = this

}


}

/**/
object user extends user_Scope0.user
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/admin/user.scala.html
                  HASH: 1f61c141e8c6ea57a7512d13703a424ef10abbea
                  MATRIX: 760->1|880->26|908->29|934->47|974->50|1002->52|1246->269|1285->299|1325->301|1357->306|1422->345|1436->350|1478->371|1510->376|1551->387|1590->398|2044->825|2057->829|2086->837|2140->864|2153->868|2183->877|2237->904|2250->908|2283->920|2712->1321|2741->1322|2773->1327|2835->1362|2863->1363|2893->1366
                  LINES: 27->1|32->1|34->3|34->3|34->3|36->5|44->13|44->13|44->13|45->14|46->15|46->15|46->15|47->16|48->17|49->18|67->36|67->36|67->36|68->37|68->37|68->37|69->38|69->38|69->38|99->68|99->68|100->69|101->70|101->70|102->71
                  -- GENERATED --
              */
          