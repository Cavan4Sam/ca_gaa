
package views.html.admin

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object fixture_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class fixture extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[models.Fixture],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(fixture: List[models.Fixture], user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.58*/("""

"""),_display_(/*3.2*/admin/*3.7*/.adminMain("Fixture", user)/*3.34*/ {_display_(Seq[Any](format.raw/*3.36*/("""
    """),format.raw/*4.5*/("""<!-- Page Content -->
    <div class="container">
        <!-- /.row -->
     <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Fixture
                    <small>List of Fixtures</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href=""""),_display_(/*14.35*/routes/*14.41*/.HomeController.index()),format.raw/*14.64*/("""">Home</a>
                    </li>
                    <li class="active">Fixture</li>
                </ol>
            </div>
        </div>

        <!-- /.row -->
	"""),_display_(/*22.3*/if(flash.containsKey("success"))/*22.35*/{_display_(Seq[Any](format.raw/*22.36*/("""
		"""),format.raw/*23.3*/("""<div class=-"alert alert-success">
			"""),_display_(/*24.5*/flash/*24.10*/.get("success")),format.raw/*24.25*/("""
		"""),format.raw/*25.3*/("""</div>
	""")))}),format.raw/*26.3*/("""
	"""),format.raw/*27.2*/("""<div class="row">
		<div class="col-lg-12">
			<table class="table table-bordered table-hover table-condensed">
				<thead>
				<!-- The header row-->
					<tr>
						<th>FixtureID</th>
						<th>Date</th>
						<th>Time</th>
						<th>Home Team</th>
						<th>Goals</th>
						<th>Points</th>
						<th>Total Points</th>
						<th>Away Team</th>
						<th>Goals</th>
						<th>Points</th>
						<th>Total Points</th>
						<th>Home Yellows</th>
						<th>Home Blacks</th>
						<th>Home Reds</th>
						<th>Away Yellows</th>
						<th>Away Blacks</th>
						<th>Away Reds</th>
					</tr>
				</thead>
				<tbody>
					<!-- Product row(s) -->
							<!-- Start of For loop - For each p in products add a row -->
				"""),_display_(/*55.6*/for(f <- fixture) yield /*55.23*/ {_display_(Seq[Any](format.raw/*55.25*/("""
					"""),format.raw/*56.6*/("""<tr>
						<td>"""),_display_(/*57.12*/f/*57.13*/.getFid),format.raw/*57.20*/("""</td>
						<td>"""),_display_(/*58.12*/f/*58.13*/.getDate),format.raw/*58.21*/("""</td>
						<td>"""),_display_(/*59.12*/f/*59.13*/.getTime),format.raw/*59.21*/("""</td>
						<td>"""),_display_(/*60.12*/f/*60.13*/.getHomeTeam),format.raw/*60.25*/("""</td>
						<td>"""),_display_(/*61.12*/f/*61.13*/.getHomeScoreGoal),format.raw/*61.30*/("""</td>
						<td>"""),_display_(/*62.12*/f/*62.13*/.getHomeScorePoint),format.raw/*62.31*/("""</td>
						<td>"""),_display_(/*63.12*/f/*63.13*/.getHomeScore),format.raw/*63.26*/("""</td>
						<td>"""),_display_(/*64.12*/f/*64.13*/.getAwayTeam),format.raw/*64.25*/("""</td>
						<td>"""),_display_(/*65.12*/f/*65.13*/.getAwayScoreGoal),format.raw/*65.30*/("""</td>
						<td>"""),_display_(/*66.12*/f/*66.13*/.getAwayScorePoint),format.raw/*66.31*/("""</td>
						<td>"""),_display_(/*67.12*/f/*67.13*/.getAwayScore),format.raw/*67.26*/("""</td>
						<td>"""),_display_(/*68.12*/f/*68.13*/.getHomeYellows),format.raw/*68.28*/("""</td>
						<td>"""),_display_(/*69.12*/f/*69.13*/.getHomeBlacks),format.raw/*69.27*/("""</td>
						<td>"""),_display_(/*70.12*/f/*70.13*/.getHomeReds),format.raw/*70.25*/("""</td>
						<td>"""),_display_(/*71.12*/f/*71.13*/.getAwayYellows),format.raw/*71.28*/("""</td>
						<td>"""),_display_(/*72.12*/f/*72.13*/.getAwayBlacks),format.raw/*72.27*/("""</td>
						<td>"""),_display_(/*73.12*/f/*73.13*/.getAwayReds),format.raw/*73.25*/("""</td>					
						<td>
							<a href=""""),_display_(/*75.18*/routes/*75.24*/.AdminController.updateFixture(f.getFid)),format.raw/*75.64*/("""" class="btn-xs btn-danger">
							<span class="glyphicon glyphicon-pencil"></span></a>
						</td>

					</tr>
							""")))}),format.raw/*80.9*/("""
				"""),format.raw/*81.5*/("""</tbody>
			</table>			
		</div>
	</div>

""")))}),format.raw/*86.2*/("""

"""))
      }
    }
  }

  def render(fixture:List[models.Fixture],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(fixture,user)

  def f:((List[models.Fixture],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (fixture,user) => apply(fixture,user)

  def ref: this.type = this

}


}

/**/
object fixture extends fixture_Scope0.fixture
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/admin/fixture.scala.html
                  HASH: ea26d4500018e0f84b398b51799640babc7a4d8a
                  MATRIX: 787->1|938->57|966->60|978->65|1013->92|1052->94|1083->99|1480->469|1495->475|1539->498|1736->669|1777->701|1816->702|1846->705|1911->744|1925->749|1961->764|1991->767|2030->776|2059->778|2802->1495|2835->1512|2875->1514|2908->1520|2951->1536|2961->1537|2989->1544|3033->1561|3043->1562|3072->1570|3116->1587|3126->1588|3155->1596|3199->1613|3209->1614|3242->1626|3286->1643|3296->1644|3334->1661|3378->1678|3388->1679|3427->1697|3471->1714|3481->1715|3515->1728|3559->1745|3569->1746|3602->1758|3646->1775|3656->1776|3694->1793|3738->1810|3748->1811|3787->1829|3831->1846|3841->1847|3875->1860|3919->1877|3929->1878|3965->1893|4009->1910|4019->1911|4054->1925|4098->1942|4108->1943|4141->1955|4185->1972|4195->1973|4231->1988|4275->2005|4285->2006|4320->2020|4364->2037|4374->2038|4407->2050|4473->2089|4488->2095|4549->2135|4700->2256|4732->2261|4805->2304
                  LINES: 27->1|32->1|34->3|34->3|34->3|34->3|35->4|45->14|45->14|45->14|53->22|53->22|53->22|54->23|55->24|55->24|55->24|56->25|57->26|58->27|86->55|86->55|86->55|87->56|88->57|88->57|88->57|89->58|89->58|89->58|90->59|90->59|90->59|91->60|91->60|91->60|92->61|92->61|92->61|93->62|93->62|93->62|94->63|94->63|94->63|95->64|95->64|95->64|96->65|96->65|96->65|97->66|97->66|97->66|98->67|98->67|98->67|99->68|99->68|99->68|100->69|100->69|100->69|101->70|101->70|101->70|102->71|102->71|102->71|103->72|103->72|103->72|104->73|104->73|104->73|106->75|106->75|106->75|111->80|112->81|117->86
                  -- GENERATED --
              */
          