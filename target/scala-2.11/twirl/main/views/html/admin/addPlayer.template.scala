
package views.html.admin

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object addPlayer_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

     object addPlayer_Scope1 {
import helper._

class addPlayer extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Form[models.Player],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*2.2*/(addPlayerForm: Form[models.Player], user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*2.63*/("""
"""),_display_(/*3.2*/admin/*3.7*/.adminMain("Add Player", user)/*3.37*/ {_display_(Seq[Any](format.raw/*3.39*/("""
	"""),format.raw/*4.2*/("""<h3>Add a new Player</h3>
	
	"""),_display_(/*6.3*/form(action = routes.AdminController.addPlayerSubmit(), 'class -> "form-horizontal", 'role->"form")/*6.102*/ {_display_(Seq[Any](format.raw/*6.104*/("""
		"""),_display_(/*7.4*/inputText(addPlayerForm("pid"), '_label -> "PlayerID", 'hidden -> "hidden")),format.raw/*7.79*/("""
		"""),_display_(/*8.4*/inputText(addPlayerForm("num"), '_label -> "Number", 'class -> "form-control")),format.raw/*8.82*/("""
		"""),_display_(/*9.4*/inputText(addPlayerForm("fname"), '_label -> "First Name", 'class -> "form-control")),format.raw/*9.88*/("""
		"""),_display_(/*10.4*/inputText(addPlayerForm("lname"), '_label -> "Last Name", 'class -> "form-control")),format.raw/*10.87*/("""
		"""),_display_(/*11.4*/inputText(addPlayerForm("pos"), '_label -> "Position", 'class -> "form-control")),format.raw/*11.84*/("""
		"""),_display_(/*12.4*/inputText(addPlayerForm("club"), '_label -> "Club", 'class -> "form-control")),format.raw/*12.81*/("""
		"""),_display_(/*13.4*/inputText(addPlayerForm("county"), '_label -> "County", 'class -> "form-control")),format.raw/*13.85*/("""
		"""),_display_(/*14.4*/inputText(addPlayerForm("age"), '_label -> "Age", 'class -> "form-control")),format.raw/*14.79*/("""
		"""),_display_(/*15.4*/inputText(addPlayerForm("apps"), '_label -> "Apperances", 'class -> "form-control")),format.raw/*15.87*/("""
		"""),_display_(/*16.4*/inputText(addPlayerForm("points"), '_label -> "Points Scored", 'class -> "form-control")),format.raw/*16.92*/("""
		"""),_display_(/*17.4*/inputText(addPlayerForm("yellows"), '_label -> "Yellow Cards", 'class -> "form-control")),format.raw/*17.92*/("""
		"""),_display_(/*18.4*/inputText(addPlayerForm("blacks"), '_label -> "Black Cards", 'class -> "form-control")),format.raw/*18.90*/("""
		"""),_display_(/*19.4*/inputText(addPlayerForm("reds"), '_label -> "Red Cards", 'class -> "form-control")),format.raw/*19.86*/("""


	"""),format.raw/*22.2*/("""<div class="actions">
		<input type="submit" value="Add Player" class="btn btn-primary">
			<a href=""""),_display_(/*24.14*/routes/*24.20*/.HomeController.index()),format.raw/*24.43*/(""""
				<button class="btn btn-warning">Cancel</button>
			</a>
	</div>
	""")))}),format.raw/*28.3*/("""
""")))}),format.raw/*29.2*/("""
"""))
      }
    }
  }

  def render(addPlayerForm:Form[models.Player],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(addPlayerForm,user)

  def f:((Form[models.Player],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (addPlayerForm,user) => apply(addPlayerForm,user)

  def ref: this.type = this

}


}
}

/**/
object addPlayer extends addPlayer_Scope0.addPlayer_Scope1.addPlayer
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/admin/addPlayer.scala.html
                  HASH: 80cc376fc82875ffa3a82ce40033ce70ba193515
                  MATRIX: 838->18|994->79|1021->81|1033->86|1071->116|1110->118|1138->120|1193->150|1301->249|1341->251|1370->255|1465->330|1494->334|1592->412|1621->416|1725->500|1755->504|1859->587|1889->591|1990->671|2020->675|2118->752|2148->756|2250->837|2280->841|2376->916|2406->920|2510->1003|2540->1007|2649->1095|2679->1099|2788->1187|2818->1191|2925->1277|2955->1281|3058->1363|3089->1367|3218->1469|3233->1475|3277->1498|3379->1570|3411->1572
                  LINES: 30->2|35->2|36->3|36->3|36->3|36->3|37->4|39->6|39->6|39->6|40->7|40->7|41->8|41->8|42->9|42->9|43->10|43->10|44->11|44->11|45->12|45->12|46->13|46->13|47->14|47->14|48->15|48->15|49->16|49->16|50->17|50->17|51->18|51->18|52->19|52->19|55->22|57->24|57->24|57->24|61->28|62->29
                  -- GENERATED --
              */
          