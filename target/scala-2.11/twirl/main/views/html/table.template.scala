
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object table_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class table extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[List[models.Team],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(leagueTable: List[models.Team],user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.58*/("""
"""),_display_(/*2.2*/main("Table", user)/*2.21*/ {_display_(Seq[Any](format.raw/*2.23*/("""


"""),format.raw/*5.1*/("""<div class="container">
    <h3>Allianz National Football League Division 1</h3>
    <hr>
    <table class="table table-bordered table-hover table-condensed">
				<thead>
				<!-- The header row-->
					<tr>
						<th>Team</th>
                        <th>Played</th>
                        <th>Won</th>
                        <th>Drew</th>
                        <th>Lost</th>
                        <th>For</th>
                        <th>Against</th>
                        <th>Difference</th>
						<th>Points</th>
					</tr>
				</thead>
				<tbody>

    """),_display_(/*25.6*/for(t <- leagueTable) yield /*25.27*/{_display_(Seq[Any](format.raw/*25.28*/("""
        """),format.raw/*26.9*/("""<tr>

            <td>"""),_display_(/*28.18*/t/*28.19*/.getName),format.raw/*28.27*/("""</td>
            <td>"""),_display_(/*29.18*/t/*29.19*/.getGamesPlayed),format.raw/*29.34*/("""</td>
            <td>"""),_display_(/*30.18*/t/*30.19*/.getGamesWon),format.raw/*30.31*/("""</td>
            <td>"""),_display_(/*31.18*/t/*31.19*/.getGamesDrew),format.raw/*31.32*/("""</td>
            <td>"""),_display_(/*32.18*/t/*32.19*/.getGamesLost),format.raw/*32.32*/("""</td>
            <td>"""),_display_(/*33.18*/t/*33.19*/.getPointsFor),format.raw/*33.32*/("""</td>
            <td>"""),_display_(/*34.18*/t/*34.19*/.getPointsAgainst),format.raw/*34.36*/("""</td>
            <td>"""),_display_(/*35.18*/t/*35.19*/.getPointsDifference),format.raw/*35.39*/("""</td>
            <td>"""),_display_(/*36.18*/t/*36.19*/.getLeaguePoints),format.raw/*36.35*/("""</td>


        
    """)))}),format.raw/*40.6*/("""
    """),format.raw/*41.5*/("""</tbody>

    </table>
    </div>
""")))}))
      }
    }
  }

  def render(leagueTable:List[models.Team],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(leagueTable,user)

  def f:((List[models.Team],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (leagueTable,user) => apply(leagueTable,user)

  def ref: this.type = this

}


}

/**/
object table extends table_Scope0.table
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/table.scala.html
                  HASH: db51a146fe93131ec64ebba5907902a636caaecd
                  MATRIX: 774->1|925->57|952->59|979->78|1018->80|1047->83|1640->650|1677->671|1716->672|1752->681|1802->704|1812->705|1841->713|1891->736|1901->737|1937->752|1987->775|1997->776|2030->788|2080->811|2090->812|2124->825|2174->848|2184->849|2218->862|2268->885|2278->886|2312->899|2362->922|2372->923|2410->940|2460->963|2470->964|2511->984|2561->1007|2571->1008|2608->1024|2660->1046|2692->1051
                  LINES: 27->1|32->1|33->2|33->2|33->2|36->5|56->25|56->25|56->25|57->26|59->28|59->28|59->28|60->29|60->29|60->29|61->30|61->30|61->30|62->31|62->31|62->31|63->32|63->32|63->32|64->33|64->33|64->33|65->34|65->34|65->34|66->35|66->35|66->35|67->36|67->36|67->36|71->40|72->41
                  -- GENERATED --
              */
          