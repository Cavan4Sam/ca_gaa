
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object results_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class results extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template3[List[models.Fixture],List[models.League],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(Results: List[models.Fixture],roundHeaders: List[models.League],user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.91*/("""
"""),_display_(/*2.2*/main("Results", user)/*2.23*/ {_display_(Seq[Any](format.raw/*2.25*/("""
  """),format.raw/*3.3*/("""<div class ="container">


    <div class="panel panel-default">

<div class="panel-heading">
    """),_display_(/*9.6*/for(h <-roundHeaders) yield /*9.27*/{_display_(Seq[Any](format.raw/*9.28*/("""
      """),format.raw/*10.7*/("""<tr>
          <h3>
          <td>"""),_display_(/*12.16*/h/*12.17*/.getName),format.raw/*12.25*/(""" """),format.raw/*12.26*/("""</td>
          <td>"""),_display_(/*13.16*/h/*13.17*/.getSponsor),format.raw/*13.28*/(""" """),format.raw/*13.29*/("""</td>
          </h3>
      </tr>
    """)))}),format.raw/*16.6*/("""
    """),format.raw/*17.5*/("""</div>

    """),_display_(/*19.6*/for(r <- Results) yield /*19.23*/ {_display_(Seq[Any](format.raw/*19.25*/("""
					"""),format.raw/*20.6*/("""<tr>
					<h5><td>"""),_display_(/*21.15*/r/*21.16*/.getDate),format.raw/*21.24*/("""</td> 
						<td>"""),_display_(/*22.12*/r/*22.13*/.getTime),format.raw/*22.21*/("""</td>
						<td>"""),_display_(/*23.12*/r/*23.13*/.getHomeTeam),format.raw/*23.25*/("""</td>
            <td>"""),_display_(/*24.18*/r/*24.19*/.getHomeScoreGoal),format.raw/*24.36*/(""" """),format.raw/*24.37*/("""-</td>
            <td>"""),_display_(/*25.18*/r/*25.19*/.getHomeScorePoint),format.raw/*25.37*/("""</td>
            <td>("""),_display_(/*26.19*/r/*26.20*/.getHomeScore),format.raw/*26.33*/(""")</td>
            <td> -v- </td>
						<td>"""),_display_(/*28.12*/r/*28.13*/.getAwayTeam),format.raw/*28.25*/("""</td>
            <td>"""),_display_(/*29.18*/r/*29.19*/.getAwayScoreGoal),format.raw/*29.36*/(""" """),format.raw/*29.37*/("""-</td>
            <td>"""),_display_(/*30.18*/r/*30.19*/.getAwayScorePoint),format.raw/*30.37*/("""</td>
            <td>("""),_display_(/*31.19*/r/*31.20*/.getAwayScore),format.raw/*31.33*/(""")</td> </h5>
          </tr>
    """)))}),format.raw/*33.6*/("""
    """),format.raw/*34.5*/("""</div>

    </div>
""")))}))
      }
    }
  }

  def render(Results:List[models.Fixture],roundHeaders:List[models.League],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(Results,roundHeaders,user)

  def f:((List[models.Fixture],List[models.League],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (Results,roundHeaders,user) => apply(Results,roundHeaders,user)

  def ref: this.type = this

}


}

/**/
object results extends results_Scope0.results
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 14:45:30 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/results.scala.html
                  HASH: cec026954bff142d6b83f6de575d3bebcfdc3806
                  MATRIX: 801->1|985->90|1012->92|1041->113|1080->115|1109->118|1233->217|1269->238|1307->239|1341->246|1403->281|1413->282|1442->290|1471->291|1519->312|1529->313|1561->324|1590->325|1659->364|1691->369|1730->382|1763->399|1803->401|1836->407|1882->426|1892->427|1921->435|1966->453|1976->454|2005->462|2049->479|2059->480|2092->492|2142->515|2152->516|2190->533|2219->534|2270->558|2280->559|2319->577|2370->601|2380->602|2414->615|2486->660|2496->661|2529->673|2579->696|2589->697|2627->714|2656->715|2707->739|2717->740|2756->758|2807->782|2817->783|2851->796|2915->830|2947->835
                  LINES: 27->1|32->1|33->2|33->2|33->2|34->3|40->9|40->9|40->9|41->10|43->12|43->12|43->12|43->12|44->13|44->13|44->13|44->13|47->16|48->17|50->19|50->19|50->19|51->20|52->21|52->21|52->21|53->22|53->22|53->22|54->23|54->23|54->23|55->24|55->24|55->24|55->24|56->25|56->25|56->25|57->26|57->26|57->26|59->28|59->28|59->28|60->29|60->29|60->29|60->29|61->30|61->30|61->30|62->31|62->31|62->31|64->33|65->34
                  -- GENERATED --
              */
          