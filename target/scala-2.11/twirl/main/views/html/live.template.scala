
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object live_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class live extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template9[List[models.Player],List[models.Player],List[models.Player],List[models.Team],List[models.Team],List[models.Team],List[models.Player],List[models.Team],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(oldestPlayer: List[models.Player],topScorer: List[models.Player], youngestPlayer: List[models.Player], worstDefence: List[models.Team], bestDefence: List[models.Team], highestScorers: List[models.Team], dirtyPlayer: List[models.Player], dirtiestTeam: List[models.Team], user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.297*/("""
	
"""),_display_(/*3.2*/main("Stats", user)/*3.21*/ {_display_(Seq[Any](format.raw/*3.23*/("""
"""),format.raw/*4.1*/("""<div class="container">

<div class="row">
        <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Highest Scorers</h4>
                    </div>
			<table class="table table-bordered table-hover table-condensed">
				<thead>
				<!-- The header row-->
					<tr>
						<th>Name</th>
						<th>County</th>
						<th>Points Total</th>

					</tr>
				</thead>
				<tbody>
					<!-- Product row(s) -->
							<!-- Start of For loop - For each p in products add a row -->
               
                
				"""),_display_(/*27.6*/for(t <- topScorer) yield /*27.25*/ {_display_(Seq[Any](format.raw/*27.27*/("""
					"""),format.raw/*28.6*/("""<tr>
						<td>"""),_display_(/*29.12*/t/*29.13*/.getFname),format.raw/*29.22*/(""" """),_display_(/*29.24*/t/*29.25*/.getLname),format.raw/*29.34*/("""</td>
						<td>"""),_display_(/*30.12*/t/*30.13*/.getCounty),format.raw/*30.23*/("""</td>
						<td>"""),_display_(/*31.12*/t/*31.13*/.getPoints),format.raw/*31.23*/(""" """),format.raw/*31.24*/("""</td>

					</tr>
                            
                            """)))}),format.raw/*35.30*/("""

				"""),format.raw/*37.5*/("""</tbody>
			</table>	
			
                </div>
			<hr>	
        </div>

                <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Oldest Players</h4>
                    </div>
			<table class="table table-bordered table-hover table-condensed">
				<thead>
				<!-- The header row-->
					<tr>
						<th>Name</th>
						<th>County</th>
						<th>Age</th>

					</tr>
				</thead>
				<tbody>
					<!-- Product row(s) -->
							<!-- Start of For loop - For each p in products add a row -->
               
                
				"""),_display_(/*64.6*/for(t <- oldestPlayer) yield /*64.28*/ {_display_(Seq[Any](format.raw/*64.30*/("""
					"""),format.raw/*65.6*/("""<tr>
						<td>"""),_display_(/*66.12*/t/*66.13*/.getFname),format.raw/*66.22*/(""" """),_display_(/*66.24*/t/*66.25*/.getLname),format.raw/*66.34*/("""</td>
						<td>"""),_display_(/*67.12*/t/*67.13*/.getCounty),format.raw/*67.23*/("""</td>
						<td>"""),_display_(/*68.12*/t/*68.13*/.getAge),format.raw/*68.20*/(""" """),format.raw/*68.21*/("""</td>

					</tr>
                            
                            """)))}),format.raw/*72.30*/("""

				"""),format.raw/*74.5*/("""</tbody>
			</table>	
			
                </div>
			<hr>	
        </div>

                <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Youngest Players</h4>
                    </div>
			<table class="table table-bordered table-hover table-condensed">
				<thead>
				<!-- The header row-->
					<tr>
						<th>Name</th>
						<th>County</th>
						<th>Age</th>

					</tr>
				</thead>
				<tbody>
					<!-- Product row(s) -->
							<!-- Start of For loop - For each p in products add a row -->
               
                
				"""),_display_(/*101.6*/for(t <- youngestPlayer) yield /*101.30*/ {_display_(Seq[Any](format.raw/*101.32*/("""
					"""),format.raw/*102.6*/("""<tr>
						<td>"""),_display_(/*103.12*/t/*103.13*/.getFname),format.raw/*103.22*/(""" """),_display_(/*103.24*/t/*103.25*/.getLname),format.raw/*103.34*/("""</td>
						<td>"""),_display_(/*104.12*/t/*104.13*/.getCounty),format.raw/*104.23*/("""</td>
						<td>"""),_display_(/*105.12*/t/*105.13*/.getAge),format.raw/*105.20*/(""" """),format.raw/*105.21*/("""</td>

					</tr>
                            
                            """)))}),format.raw/*109.30*/("""

				"""),format.raw/*111.5*/("""</tbody>
			</table>	
			
                </div>
			<hr>	
        </div>


    </div>


    <div class ="row">


        <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Worst Defence</h4>
                    </div>
			<table class="table table-bordered table-hover table-condensed">
				<thead>
				<!-- The header row-->
					<tr>
						<th>Name</th>
						<th>Provence</th>
                        <th>Points Conceeded</th>

					</tr>
				</thead>
				<tbody>
					<!-- Product row(s) -->
							<!-- Start of For loop - For each p in products add a row -->
               
                
				"""),_display_(/*145.6*/for(t <- worstDefence) yield /*145.28*/ {_display_(Seq[Any](format.raw/*145.30*/("""
					"""),format.raw/*146.6*/("""<tr>
						<td>"""),_display_(/*147.12*/t/*147.13*/.getName),format.raw/*147.21*/("""</td>
						<td>"""),_display_(/*148.12*/t/*148.13*/.getProv),format.raw/*148.21*/("""</td>
						<td>"""),_display_(/*149.12*/t/*149.13*/.getPointsAgainst),format.raw/*149.30*/(""" """),format.raw/*149.31*/("""</td>

					</tr>
                            
                            """)))}),format.raw/*153.30*/("""

				"""),format.raw/*155.5*/("""</tbody>
			</table>	
			
                </div>
			<hr>	
        </div>

                <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Best Defence</h4>
                    </div>
			<table class="table table-bordered table-hover table-condensed">
				<thead>
				<!-- The header row-->
					<tr>
						<th>Name</th>
						<th>Provience</th>
						<th>Points Conceeded</th>

					</tr>
				</thead>
				<tbody>
					<!-- Product row(s) -->
							<!-- Start of For loop - For each p in products add a row -->
               
                
				"""),_display_(/*182.6*/for(t <- bestDefence) yield /*182.27*/ {_display_(Seq[Any](format.raw/*182.29*/("""
					"""),format.raw/*183.6*/("""<tr>
						<td>"""),_display_(/*184.12*/t/*184.13*/.getName),format.raw/*184.21*/("""</td>
						<td>"""),_display_(/*185.12*/t/*185.13*/.getProv),format.raw/*185.21*/("""</td>
						<td>"""),_display_(/*186.12*/t/*186.13*/.getPointsAgainst),format.raw/*186.30*/(""" """),format.raw/*186.31*/("""</td>

					</tr>
                            
                            """)))}),format.raw/*190.30*/("""

				"""),format.raw/*192.5*/("""</tbody>
			</table>	
			
                </div>
			<hr>	
        </div>

                <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Top Scoring Team</h4>
                    </div>
			<table class="table table-bordered table-hover table-condensed">
				<thead>
				<!-- The header row-->
					<tr>
						<th>Name</th>
						<th>Provience</th>
						<th>Points Total</th>

					</tr>
				</thead>
				<tbody>
					<!-- Product row(s) -->
							<!-- Start of For loop - For each p in products add a row -->
               
                
				"""),_display_(/*219.6*/for(t <- highestScorers) yield /*219.30*/ {_display_(Seq[Any](format.raw/*219.32*/("""
					"""),format.raw/*220.6*/("""<tr>
						<td>"""),_display_(/*221.12*/t/*221.13*/.getName),format.raw/*221.21*/("""</td>
						<td>"""),_display_(/*222.12*/t/*222.13*/.getProv),format.raw/*222.21*/("""</td>
						<td>"""),_display_(/*223.12*/t/*223.13*/.getPointsFor),format.raw/*223.26*/(""" """),format.raw/*223.27*/("""</td>

					</tr>
                            
                            """)))}),format.raw/*227.30*/("""

				"""),format.raw/*229.5*/("""</tbody>
			</table>	
			
                </div>
			<hr>	
        </div>




    </div>

    <div class="row">

                        <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Dirtiest Teams</h4>
                    </div>
			<table class="table table-bordered table-hover table-condensed">
				<thead>
				<!-- The header row-->
					<tr>
						<th>Name</th>
						<th>Prov</th>
						<th>Yellow Cards</th>
                        <th>Black Cards</th>
                        <th>Red Cards</th>

					</tr>
				</thead>
				<tbody>
					<!-- Product row(s) -->
							<!-- Start of For loop - For each p in products add a row -->
               
                
				"""),_display_(/*265.6*/for(t <- dirtiestTeam) yield /*265.28*/ {_display_(Seq[Any](format.raw/*265.30*/("""
					"""),format.raw/*266.6*/("""<tr>
						<td>"""),_display_(/*267.12*/t/*267.13*/.getName),format.raw/*267.21*/("""</td>
						<td>"""),_display_(/*268.12*/t/*268.13*/.getProv),format.raw/*268.21*/("""</td>
						<td>"""),_display_(/*269.12*/t/*269.13*/.getYellows),format.raw/*269.24*/(""" """),format.raw/*269.25*/("""</td>
                        <td>"""),_display_(/*270.30*/t/*270.31*/.getBlacks),format.raw/*270.41*/(""" """),format.raw/*270.42*/("""</td>
                        <td>"""),_display_(/*271.30*/t/*271.31*/.getReds),format.raw/*271.39*/(""" """),format.raw/*271.40*/("""</td>

					</tr>
                            
                            """)))}),format.raw/*275.30*/("""

				"""),format.raw/*277.5*/("""</tbody>
			</table>	
			
                </div>
			<hr>	
        </div>





                        <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Dirtiest Players</h4>
                    </div>
			<table class="table table-bordered table-hover table-condensed">
				<thead>
				<!-- The header row-->
					<tr>
						<th>Name</th>
						<th>County</th>
						<th>Yellow Cards</th>
                        <th>Black Cards</th>
                        <th>Red Cards</th>

					</tr>
				</thead>
				<tbody>
					<!-- Product row(s) -->
							<!-- Start of For loop - For each p in products add a row -->
               
                
				"""),_display_(/*310.6*/for(t <- dirtyPlayer) yield /*310.27*/ {_display_(Seq[Any](format.raw/*310.29*/("""
					"""),format.raw/*311.6*/("""<tr>
						<td>"""),_display_(/*312.12*/t/*312.13*/.getFname),format.raw/*312.22*/(""" """),_display_(/*312.24*/t/*312.25*/.getLname),format.raw/*312.34*/("""</td>
						<td>"""),_display_(/*313.12*/t/*313.13*/.getCounty),format.raw/*313.23*/("""</td>
						<td>"""),_display_(/*314.12*/t/*314.13*/.getYellows),format.raw/*314.24*/(""" """),format.raw/*314.25*/("""</td>
                        <td>"""),_display_(/*315.30*/t/*315.31*/.getBlacks),format.raw/*315.41*/(""" """),format.raw/*315.42*/("""</td>
                        <td>"""),_display_(/*316.30*/t/*316.31*/.getReds),format.raw/*316.39*/(""" """),format.raw/*316.40*/("""</td>

					</tr>
                            
                            """)))}),format.raw/*320.30*/("""

				"""),format.raw/*322.5*/("""</tbody>
			</table>	
			
                </div>
			<hr>	
        </div>




    </div>



            
</div>
""")))}),format.raw/*338.2*/("""
"""))
      }
    }
  }

  def render(oldestPlayer:List[models.Player],topScorer:List[models.Player],youngestPlayer:List[models.Player],worstDefence:List[models.Team],bestDefence:List[models.Team],highestScorers:List[models.Team],dirtyPlayer:List[models.Player],dirtiestTeam:List[models.Team],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(oldestPlayer,topScorer,youngestPlayer,worstDefence,bestDefence,highestScorers,dirtyPlayer,dirtiestTeam,user)

  def f:((List[models.Player],List[models.Player],List[models.Player],List[models.Team],List[models.Team],List[models.Team],List[models.Player],List[models.Team],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (oldestPlayer,topScorer,youngestPlayer,worstDefence,bestDefence,highestScorers,dirtyPlayer,dirtiestTeam,user) => apply(oldestPlayer,topScorer,youngestPlayer,worstDefence,bestDefence,highestScorers,dirtyPlayer,dirtiestTeam,user)

  def ref: this.type = this

}


}

/**/
object live extends live_Scope0.live
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/live.scala.html
                  HASH: 29f7d4824f7d13a1aacbfd0ff3ccbc999a6b8ff8
                  MATRIX: 906->1|1297->296|1326->300|1353->319|1392->321|1419->322|2053->930|2088->949|2128->951|2161->957|2204->973|2214->974|2244->983|2273->985|2283->986|2313->995|2357->1012|2367->1013|2398->1023|2442->1040|2452->1041|2483->1051|2512->1052|2619->1128|2652->1134|3315->1771|3353->1793|3393->1795|3426->1801|3469->1817|3479->1818|3509->1827|3538->1829|3548->1830|3578->1839|3622->1856|3632->1857|3663->1867|3707->1884|3717->1885|3745->1892|3774->1893|3881->1969|3914->1975|4580->2614|4621->2638|4662->2640|4696->2646|4740->2662|4751->2663|4782->2672|4812->2674|4823->2675|4854->2684|4899->2701|4910->2702|4942->2712|4987->2729|4998->2730|5027->2737|5057->2738|5165->2814|5199->2820|5926->3520|5965->3542|6006->3544|6040->3550|6084->3566|6095->3567|6125->3575|6170->3592|6181->3593|6211->3601|6256->3618|6267->3619|6306->3636|6336->3637|6444->3713|6478->3719|7156->4370|7194->4391|7235->4393|7269->4399|7313->4415|7324->4416|7354->4424|7399->4441|7410->4442|7440->4450|7485->4467|7496->4468|7535->4485|7565->4486|7673->4562|7707->4568|8385->5219|8426->5243|8467->5245|8501->5251|8545->5267|8556->5268|8586->5276|8631->5293|8642->5294|8672->5302|8717->5319|8728->5320|8763->5333|8793->5334|8901->5410|8935->5416|9740->6194|9779->6216|9820->6218|9854->6224|9898->6240|9909->6241|9939->6249|9984->6266|9995->6267|10025->6275|10070->6292|10081->6293|10114->6304|10144->6305|10207->6340|10218->6341|10250->6351|10280->6352|10343->6387|10354->6388|10384->6396|10414->6397|10522->6473|10556->6479|11331->7227|11369->7248|11410->7250|11444->7256|11488->7272|11499->7273|11530->7282|11560->7284|11571->7285|11602->7294|11647->7311|11658->7312|11690->7322|11735->7339|11746->7340|11779->7351|11809->7352|11872->7387|11883->7388|11915->7398|11945->7399|12008->7434|12019->7435|12049->7443|12079->7444|12187->7520|12221->7526|12364->7638
                  LINES: 27->1|32->1|34->3|34->3|34->3|35->4|58->27|58->27|58->27|59->28|60->29|60->29|60->29|60->29|60->29|60->29|61->30|61->30|61->30|62->31|62->31|62->31|62->31|66->35|68->37|95->64|95->64|95->64|96->65|97->66|97->66|97->66|97->66|97->66|97->66|98->67|98->67|98->67|99->68|99->68|99->68|99->68|103->72|105->74|132->101|132->101|132->101|133->102|134->103|134->103|134->103|134->103|134->103|134->103|135->104|135->104|135->104|136->105|136->105|136->105|136->105|140->109|142->111|176->145|176->145|176->145|177->146|178->147|178->147|178->147|179->148|179->148|179->148|180->149|180->149|180->149|180->149|184->153|186->155|213->182|213->182|213->182|214->183|215->184|215->184|215->184|216->185|216->185|216->185|217->186|217->186|217->186|217->186|221->190|223->192|250->219|250->219|250->219|251->220|252->221|252->221|252->221|253->222|253->222|253->222|254->223|254->223|254->223|254->223|258->227|260->229|296->265|296->265|296->265|297->266|298->267|298->267|298->267|299->268|299->268|299->268|300->269|300->269|300->269|300->269|301->270|301->270|301->270|301->270|302->271|302->271|302->271|302->271|306->275|308->277|341->310|341->310|341->310|342->311|343->312|343->312|343->312|343->312|343->312|343->312|344->313|344->313|344->313|345->314|345->314|345->314|345->314|346->315|346->315|346->315|346->315|347->316|347->316|347->316|347->316|351->320|353->322|369->338
                  -- GENERATED --
              */
          