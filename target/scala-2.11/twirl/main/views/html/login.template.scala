
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object login_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class login extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Form[models.users.Login],models.users.User,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(loginForm: Form[models.users.Login], user: models.users.User):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {
import helper._

Seq[Any](format.raw/*1.64*/("""
"""),format.raw/*3.1*/("""	
"""),_display_(/*4.2*/main("Login", user)/*4.21*/ {_display_(Seq[Any](format.raw/*4.23*/("""

"""),format.raw/*6.1*/("""<div class="row">
	<div class ="col-xs-4">
		</div>
		
		<div class="col-xs-4">
			<h3>Sign In</h3>
			"""),_display_(/*12.5*/if(loginForm.hasGlobalErrors)/*12.34*/ {_display_(Seq[Any](format.raw/*12.36*/("""
				"""),format.raw/*13.5*/("""<p class="alert alert-warning">
				"""),_display_(/*14.6*/loginForm/*14.15*/.globalError.message),format.raw/*14.35*/("""
				"""),format.raw/*15.5*/("""</p>
			""")))}),format.raw/*16.5*/("""
			"""),_display_(/*17.5*/if(flash.containsKey("error"))/*17.35*/ {_display_(Seq[Any](format.raw/*17.37*/("""
				"""),format.raw/*18.5*/("""<div class="alert alert-warning">
				"""),_display_(/*19.6*/flash/*19.11*/.get("loginRequired")),format.raw/*19.32*/("""
				"""),format.raw/*20.5*/("""</div>
			""")))}),format.raw/*21.5*/("""
			"""),_display_(/*22.5*/helper/*22.11*/.form(action = controllers.security.routes.LoginController.loginSubmit())/*22.84*/ {_display_(Seq[Any](format.raw/*22.86*/("""
				"""),format.raw/*23.5*/("""<div class="form-group">
					"""),_display_(/*24.7*/inputText(loginForm("email"), '_label -> "", 'class -> "form-control input-xs", 'placeholder -> "Email")),format.raw/*24.111*/("""
				"""),format.raw/*25.5*/("""</div>
				<div class="form-group">
					"""),_display_(/*27.7*/inputPassword(loginForm("password"), '_label -> "", 'class -> "form-control input-xs", 'placeholder -> "Password")),format.raw/*27.121*/("""
				"""),format.raw/*28.5*/("""</div>
				<div class="form-group">
					<input type="submit" value="Sign In" class="btn btn-primary">
				</div>
			""")))}),format.raw/*32.5*/("""

           """),format.raw/*34.12*/("""</div> 

		   </div>
	""")))}))
      }
    }
  }

  def render(loginForm:Form[models.users.Login],user:models.users.User): play.twirl.api.HtmlFormat.Appendable = apply(loginForm,user)

  def f:((Form[models.users.Login],models.users.User) => play.twirl.api.HtmlFormat.Appendable) = (loginForm,user) => apply(loginForm,user)

  def ref: this.type = this

}


}

/**/
object login extends login_Scope0.login
              /*
                  -- GENERATED --
                  DATE: Mon Apr 24 07:17:31 IST 2017
                  SOURCE: /home/wdd/webapps/ca_gaa/app/views/login.scala.html
                  HASH: bd7892b7ab20d4f76fe7bf936a7bb8fa81a833ed
                  MATRIX: 781->1|953->63|980->81|1008->84|1035->103|1074->105|1102->107|1232->211|1270->240|1310->242|1342->247|1405->284|1423->293|1464->313|1496->318|1535->327|1566->332|1605->362|1645->364|1677->369|1742->408|1756->413|1798->434|1830->439|1871->450|1902->455|1917->461|1999->534|2039->536|2071->541|2128->572|2254->676|2286->681|2354->723|2490->837|2522->842|2670->960|2711->973
                  LINES: 27->1|32->1|33->3|34->4|34->4|34->4|36->6|42->12|42->12|42->12|43->13|44->14|44->14|44->14|45->15|46->16|47->17|47->17|47->17|48->18|49->19|49->19|49->19|50->20|51->21|52->22|52->22|52->22|52->22|53->23|54->24|54->24|55->25|57->27|57->27|58->28|62->32|64->34
                  -- GENERATED --
              */
          