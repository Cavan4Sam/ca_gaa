
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/wdd/webapps/ca_gaa/conf/routes
// @DATE:Mon Apr 24 07:17:30 IST 2017

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  HomeController_2: controllers.HomeController,
  // @LINE:15
  AdminController_0: controllers.AdminController,
  // @LINE:44
  LoginController_5: controllers.security.LoginController,
  // @LINE:52
  CountController_1: controllers.CountController,
  // @LINE:54
  AsyncController_3: controllers.AsyncController,
  // @LINE:57
  Assets_4: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    HomeController_2: controllers.HomeController,
    // @LINE:15
    AdminController_0: controllers.AdminController,
    // @LINE:44
    LoginController_5: controllers.security.LoginController,
    // @LINE:52
    CountController_1: controllers.CountController,
    // @LINE:54
    AsyncController_3: controllers.AsyncController,
    // @LINE:57
    Assets_4: controllers.Assets
  ) = this(errorHandler, HomeController_2, AdminController_0, LoginController_5, CountController_1, AsyncController_3, Assets_4, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, HomeController_2, AdminController_0, LoginController_5, CountController_1, AsyncController_3, Assets_4, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.HomeController.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """live""", """controllers.HomeController.live"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """results""", """controllers.HomeController.results"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """fixtures""", """controllers.HomeController.fixtures"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """table""", """controllers.HomeController.table"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/""", """controllers.AdminController.adminIndex"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/fixture""", """controllers.AdminController.fixture"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/league""", """controllers.AdminController.league"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/player""", """controllers.AdminController.player"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/team""", """controllers.AdminController.team"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/referee""", """controllers.AdminController.referee"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/addPlayer""", """controllers.AdminController.addPlayer"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/addPlayerSubmit""", """controllers.AdminController.addPlayerSubmit"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """delPlayer/""" + "$" + """pid<[^/]+>""", """controllers.AdminController.deletePlayer(pid:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/updatePlayer/""" + "$" + """pid<[^/]+>""", """controllers.AdminController.updatePlayer(pid:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/addTeam""", """controllers.AdminController.addTeam"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/addTeamSubmit""", """controllers.AdminController.addTeamSubmit"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/delTeam/""" + "$" + """tid<[^/]+>""", """controllers.AdminController.deleteTeam(tid:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/updateTeam/""" + "$" + """tid<[^/]+>""", """controllers.AdminController.updateTeam(tid:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/addFixture""", """controllers.AdminController.addFixture"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/addFixtureSubmit""", """controllers.AdminController.addFixtureSubmit"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/delFixture/""" + "$" + """fid<[^/]+>""", """controllers.AdminController.deleteFixture(fid:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/updateFixture/""" + "$" + """fid<[^/]+>""", """controllers.AdminController.updateFixture(fid:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/addReferee""", """controllers.AdminController.addReferee"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/addRefereeSubmit""", """controllers.AdminController.addRefereeSubmit"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/delReferee/""" + "$" + """rid<[^/]+>""", """controllers.AdminController.deleteReferee(rid:Long)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/controlGame""", """controllers.AdminController.controlGame"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """admin/incHomeGoal/""", """controllers.AdminController.incHomeGoal"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """login""", """controllers.security.LoginController.login"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """loginSubmit""", """controllers.security.LoginController.loginSubmit"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """logout""", """controllers.security.LoginController.logout"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """count""", """controllers.CountController.count"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """message""", """controllers.AsyncController.message"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_HomeController_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_HomeController_index0_invoker = createInvoker(
    HomeController_2.index,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "index",
      Nil,
      "GET",
      """""",
      this.prefix + """"""
    )
  )

  // @LINE:7
  private[this] lazy val controllers_HomeController_live1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("live")))
  )
  private[this] lazy val controllers_HomeController_live1_invoker = createInvoker(
    HomeController_2.live,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "live",
      Nil,
      "GET",
      """""",
      this.prefix + """live"""
    )
  )

  // @LINE:8
  private[this] lazy val controllers_HomeController_results2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("results")))
  )
  private[this] lazy val controllers_HomeController_results2_invoker = createInvoker(
    HomeController_2.results,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "results",
      Nil,
      "GET",
      """""",
      this.prefix + """results"""
    )
  )

  // @LINE:9
  private[this] lazy val controllers_HomeController_fixtures3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("fixtures")))
  )
  private[this] lazy val controllers_HomeController_fixtures3_invoker = createInvoker(
    HomeController_2.fixtures,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "fixtures",
      Nil,
      "GET",
      """""",
      this.prefix + """fixtures"""
    )
  )

  // @LINE:10
  private[this] lazy val controllers_HomeController_table4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("table")))
  )
  private[this] lazy val controllers_HomeController_table4_invoker = createInvoker(
    HomeController_2.table,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "table",
      Nil,
      "GET",
      """""",
      this.prefix + """table"""
    )
  )

  // @LINE:15
  private[this] lazy val controllers_AdminController_adminIndex5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/")))
  )
  private[this] lazy val controllers_AdminController_adminIndex5_invoker = createInvoker(
    AdminController_0.adminIndex,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "adminIndex",
      Nil,
      "GET",
      """""",
      this.prefix + """admin/"""
    )
  )

  // @LINE:16
  private[this] lazy val controllers_AdminController_fixture6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/fixture")))
  )
  private[this] lazy val controllers_AdminController_fixture6_invoker = createInvoker(
    AdminController_0.fixture,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "fixture",
      Nil,
      "GET",
      """""",
      this.prefix + """admin/fixture"""
    )
  )

  // @LINE:17
  private[this] lazy val controllers_AdminController_league7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/league")))
  )
  private[this] lazy val controllers_AdminController_league7_invoker = createInvoker(
    AdminController_0.league,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "league",
      Nil,
      "GET",
      """""",
      this.prefix + """admin/league"""
    )
  )

  // @LINE:18
  private[this] lazy val controllers_AdminController_player8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/player")))
  )
  private[this] lazy val controllers_AdminController_player8_invoker = createInvoker(
    AdminController_0.player,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "player",
      Nil,
      "GET",
      """""",
      this.prefix + """admin/player"""
    )
  )

  // @LINE:19
  private[this] lazy val controllers_AdminController_team9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/team")))
  )
  private[this] lazy val controllers_AdminController_team9_invoker = createInvoker(
    AdminController_0.team,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "team",
      Nil,
      "GET",
      """""",
      this.prefix + """admin/team"""
    )
  )

  // @LINE:20
  private[this] lazy val controllers_AdminController_referee10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/referee")))
  )
  private[this] lazy val controllers_AdminController_referee10_invoker = createInvoker(
    AdminController_0.referee,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "referee",
      Nil,
      "GET",
      """""",
      this.prefix + """admin/referee"""
    )
  )

  // @LINE:21
  private[this] lazy val controllers_AdminController_addPlayer11_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/addPlayer")))
  )
  private[this] lazy val controllers_AdminController_addPlayer11_invoker = createInvoker(
    AdminController_0.addPlayer,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "addPlayer",
      Nil,
      "GET",
      """""",
      this.prefix + """admin/addPlayer"""
    )
  )

  // @LINE:22
  private[this] lazy val controllers_AdminController_addPlayerSubmit12_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/addPlayerSubmit")))
  )
  private[this] lazy val controllers_AdminController_addPlayerSubmit12_invoker = createInvoker(
    AdminController_0.addPlayerSubmit,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "addPlayerSubmit",
      Nil,
      "POST",
      """""",
      this.prefix + """admin/addPlayerSubmit"""
    )
  )

  // @LINE:23
  private[this] lazy val controllers_AdminController_deletePlayer13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("delPlayer/"), DynamicPart("pid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminController_deletePlayer13_invoker = createInvoker(
    AdminController_0.deletePlayer(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "deletePlayer",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """delPlayer/""" + "$" + """pid<[^/]+>"""
    )
  )

  // @LINE:24
  private[this] lazy val controllers_AdminController_updatePlayer14_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/updatePlayer/"), DynamicPart("pid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminController_updatePlayer14_invoker = createInvoker(
    AdminController_0.updatePlayer(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "updatePlayer",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """admin/updatePlayer/""" + "$" + """pid<[^/]+>"""
    )
  )

  // @LINE:25
  private[this] lazy val controllers_AdminController_addTeam15_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/addTeam")))
  )
  private[this] lazy val controllers_AdminController_addTeam15_invoker = createInvoker(
    AdminController_0.addTeam,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "addTeam",
      Nil,
      "GET",
      """""",
      this.prefix + """admin/addTeam"""
    )
  )

  // @LINE:26
  private[this] lazy val controllers_AdminController_addTeamSubmit16_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/addTeamSubmit")))
  )
  private[this] lazy val controllers_AdminController_addTeamSubmit16_invoker = createInvoker(
    AdminController_0.addTeamSubmit,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "addTeamSubmit",
      Nil,
      "POST",
      """""",
      this.prefix + """admin/addTeamSubmit"""
    )
  )

  // @LINE:27
  private[this] lazy val controllers_AdminController_deleteTeam17_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/delTeam/"), DynamicPart("tid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminController_deleteTeam17_invoker = createInvoker(
    AdminController_0.deleteTeam(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "deleteTeam",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """admin/delTeam/""" + "$" + """tid<[^/]+>"""
    )
  )

  // @LINE:28
  private[this] lazy val controllers_AdminController_updateTeam18_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/updateTeam/"), DynamicPart("tid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminController_updateTeam18_invoker = createInvoker(
    AdminController_0.updateTeam(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "updateTeam",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """admin/updateTeam/""" + "$" + """tid<[^/]+>"""
    )
  )

  // @LINE:29
  private[this] lazy val controllers_AdminController_addFixture19_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/addFixture")))
  )
  private[this] lazy val controllers_AdminController_addFixture19_invoker = createInvoker(
    AdminController_0.addFixture,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "addFixture",
      Nil,
      "GET",
      """""",
      this.prefix + """admin/addFixture"""
    )
  )

  // @LINE:30
  private[this] lazy val controllers_AdminController_addFixtureSubmit20_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/addFixtureSubmit")))
  )
  private[this] lazy val controllers_AdminController_addFixtureSubmit20_invoker = createInvoker(
    AdminController_0.addFixtureSubmit,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "addFixtureSubmit",
      Nil,
      "POST",
      """""",
      this.prefix + """admin/addFixtureSubmit"""
    )
  )

  // @LINE:31
  private[this] lazy val controllers_AdminController_deleteFixture21_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/delFixture/"), DynamicPart("fid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminController_deleteFixture21_invoker = createInvoker(
    AdminController_0.deleteFixture(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "deleteFixture",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """admin/delFixture/""" + "$" + """fid<[^/]+>"""
    )
  )

  // @LINE:32
  private[this] lazy val controllers_AdminController_updateFixture22_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/updateFixture/"), DynamicPart("fid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminController_updateFixture22_invoker = createInvoker(
    AdminController_0.updateFixture(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "updateFixture",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """admin/updateFixture/""" + "$" + """fid<[^/]+>"""
    )
  )

  // @LINE:33
  private[this] lazy val controllers_AdminController_addReferee23_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/addReferee")))
  )
  private[this] lazy val controllers_AdminController_addReferee23_invoker = createInvoker(
    AdminController_0.addReferee,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "addReferee",
      Nil,
      "GET",
      """""",
      this.prefix + """admin/addReferee"""
    )
  )

  // @LINE:34
  private[this] lazy val controllers_AdminController_addRefereeSubmit24_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/addRefereeSubmit")))
  )
  private[this] lazy val controllers_AdminController_addRefereeSubmit24_invoker = createInvoker(
    AdminController_0.addRefereeSubmit,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "addRefereeSubmit",
      Nil,
      "POST",
      """""",
      this.prefix + """admin/addRefereeSubmit"""
    )
  )

  // @LINE:35
  private[this] lazy val controllers_AdminController_deleteReferee25_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/delReferee/"), DynamicPart("rid", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AdminController_deleteReferee25_invoker = createInvoker(
    AdminController_0.deleteReferee(fakeValue[Long]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "deleteReferee",
      Seq(classOf[Long]),
      "GET",
      """""",
      this.prefix + """admin/delReferee/""" + "$" + """rid<[^/]+>"""
    )
  )

  // @LINE:38
  private[this] lazy val controllers_AdminController_controlGame26_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/controlGame")))
  )
  private[this] lazy val controllers_AdminController_controlGame26_invoker = createInvoker(
    AdminController_0.controlGame,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "controlGame",
      Nil,
      "GET",
      """""",
      this.prefix + """admin/controlGame"""
    )
  )

  // @LINE:39
  private[this] lazy val controllers_AdminController_incHomeGoal27_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("admin/incHomeGoal/")))
  )
  private[this] lazy val controllers_AdminController_incHomeGoal27_invoker = createInvoker(
    AdminController_0.incHomeGoal,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AdminController",
      "incHomeGoal",
      Nil,
      "GET",
      """""",
      this.prefix + """admin/incHomeGoal/"""
    )
  )

  // @LINE:44
  private[this] lazy val controllers_security_LoginController_login28_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("login")))
  )
  private[this] lazy val controllers_security_LoginController_login28_invoker = createInvoker(
    LoginController_5.login,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.security.LoginController",
      "login",
      Nil,
      "GET",
      """""",
      this.prefix + """login"""
    )
  )

  // @LINE:45
  private[this] lazy val controllers_security_LoginController_loginSubmit29_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("loginSubmit")))
  )
  private[this] lazy val controllers_security_LoginController_loginSubmit29_invoker = createInvoker(
    LoginController_5.loginSubmit,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.security.LoginController",
      "loginSubmit",
      Nil,
      "POST",
      """""",
      this.prefix + """loginSubmit"""
    )
  )

  // @LINE:46
  private[this] lazy val controllers_security_LoginController_logout30_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("logout")))
  )
  private[this] lazy val controllers_security_LoginController_logout30_invoker = createInvoker(
    LoginController_5.logout,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.security.LoginController",
      "logout",
      Nil,
      "GET",
      """""",
      this.prefix + """logout"""
    )
  )

  // @LINE:52
  private[this] lazy val controllers_CountController_count31_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("count")))
  )
  private[this] lazy val controllers_CountController_count31_invoker = createInvoker(
    CountController_1.count,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.CountController",
      "count",
      Nil,
      "GET",
      """ An example controller showing how to use dependency injection""",
      this.prefix + """count"""
    )
  )

  // @LINE:54
  private[this] lazy val controllers_AsyncController_message32_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("message")))
  )
  private[this] lazy val controllers_AsyncController_message32_invoker = createInvoker(
    AsyncController_3.message,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AsyncController",
      "message",
      Nil,
      "GET",
      """ An example controller showing how to write asynchronous code""",
      this.prefix + """message"""
    )
  )

  // @LINE:57
  private[this] lazy val controllers_Assets_versioned33_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned33_invoker = createInvoker(
    Assets_4.versioned(fakeValue[String], fakeValue[Asset]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      """ Map static resources from the /public folder to the /assets URL path""",
      this.prefix + """assets/""" + "$" + """file<.+>"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_HomeController_index0_route(params) =>
      call { 
        controllers_HomeController_index0_invoker.call(HomeController_2.index)
      }
  
    // @LINE:7
    case controllers_HomeController_live1_route(params) =>
      call { 
        controllers_HomeController_live1_invoker.call(HomeController_2.live)
      }
  
    // @LINE:8
    case controllers_HomeController_results2_route(params) =>
      call { 
        controllers_HomeController_results2_invoker.call(HomeController_2.results)
      }
  
    // @LINE:9
    case controllers_HomeController_fixtures3_route(params) =>
      call { 
        controllers_HomeController_fixtures3_invoker.call(HomeController_2.fixtures)
      }
  
    // @LINE:10
    case controllers_HomeController_table4_route(params) =>
      call { 
        controllers_HomeController_table4_invoker.call(HomeController_2.table)
      }
  
    // @LINE:15
    case controllers_AdminController_adminIndex5_route(params) =>
      call { 
        controllers_AdminController_adminIndex5_invoker.call(AdminController_0.adminIndex)
      }
  
    // @LINE:16
    case controllers_AdminController_fixture6_route(params) =>
      call { 
        controllers_AdminController_fixture6_invoker.call(AdminController_0.fixture)
      }
  
    // @LINE:17
    case controllers_AdminController_league7_route(params) =>
      call { 
        controllers_AdminController_league7_invoker.call(AdminController_0.league)
      }
  
    // @LINE:18
    case controllers_AdminController_player8_route(params) =>
      call { 
        controllers_AdminController_player8_invoker.call(AdminController_0.player)
      }
  
    // @LINE:19
    case controllers_AdminController_team9_route(params) =>
      call { 
        controllers_AdminController_team9_invoker.call(AdminController_0.team)
      }
  
    // @LINE:20
    case controllers_AdminController_referee10_route(params) =>
      call { 
        controllers_AdminController_referee10_invoker.call(AdminController_0.referee)
      }
  
    // @LINE:21
    case controllers_AdminController_addPlayer11_route(params) =>
      call { 
        controllers_AdminController_addPlayer11_invoker.call(AdminController_0.addPlayer)
      }
  
    // @LINE:22
    case controllers_AdminController_addPlayerSubmit12_route(params) =>
      call { 
        controllers_AdminController_addPlayerSubmit12_invoker.call(AdminController_0.addPlayerSubmit)
      }
  
    // @LINE:23
    case controllers_AdminController_deletePlayer13_route(params) =>
      call(params.fromPath[Long]("pid", None)) { (pid) =>
        controllers_AdminController_deletePlayer13_invoker.call(AdminController_0.deletePlayer(pid))
      }
  
    // @LINE:24
    case controllers_AdminController_updatePlayer14_route(params) =>
      call(params.fromPath[Long]("pid", None)) { (pid) =>
        controllers_AdminController_updatePlayer14_invoker.call(AdminController_0.updatePlayer(pid))
      }
  
    // @LINE:25
    case controllers_AdminController_addTeam15_route(params) =>
      call { 
        controllers_AdminController_addTeam15_invoker.call(AdminController_0.addTeam)
      }
  
    // @LINE:26
    case controllers_AdminController_addTeamSubmit16_route(params) =>
      call { 
        controllers_AdminController_addTeamSubmit16_invoker.call(AdminController_0.addTeamSubmit)
      }
  
    // @LINE:27
    case controllers_AdminController_deleteTeam17_route(params) =>
      call(params.fromPath[Long]("tid", None)) { (tid) =>
        controllers_AdminController_deleteTeam17_invoker.call(AdminController_0.deleteTeam(tid))
      }
  
    // @LINE:28
    case controllers_AdminController_updateTeam18_route(params) =>
      call(params.fromPath[Long]("tid", None)) { (tid) =>
        controllers_AdminController_updateTeam18_invoker.call(AdminController_0.updateTeam(tid))
      }
  
    // @LINE:29
    case controllers_AdminController_addFixture19_route(params) =>
      call { 
        controllers_AdminController_addFixture19_invoker.call(AdminController_0.addFixture)
      }
  
    // @LINE:30
    case controllers_AdminController_addFixtureSubmit20_route(params) =>
      call { 
        controllers_AdminController_addFixtureSubmit20_invoker.call(AdminController_0.addFixtureSubmit)
      }
  
    // @LINE:31
    case controllers_AdminController_deleteFixture21_route(params) =>
      call(params.fromPath[Long]("fid", None)) { (fid) =>
        controllers_AdminController_deleteFixture21_invoker.call(AdminController_0.deleteFixture(fid))
      }
  
    // @LINE:32
    case controllers_AdminController_updateFixture22_route(params) =>
      call(params.fromPath[Long]("fid", None)) { (fid) =>
        controllers_AdminController_updateFixture22_invoker.call(AdminController_0.updateFixture(fid))
      }
  
    // @LINE:33
    case controllers_AdminController_addReferee23_route(params) =>
      call { 
        controllers_AdminController_addReferee23_invoker.call(AdminController_0.addReferee)
      }
  
    // @LINE:34
    case controllers_AdminController_addRefereeSubmit24_route(params) =>
      call { 
        controllers_AdminController_addRefereeSubmit24_invoker.call(AdminController_0.addRefereeSubmit)
      }
  
    // @LINE:35
    case controllers_AdminController_deleteReferee25_route(params) =>
      call(params.fromPath[Long]("rid", None)) { (rid) =>
        controllers_AdminController_deleteReferee25_invoker.call(AdminController_0.deleteReferee(rid))
      }
  
    // @LINE:38
    case controllers_AdminController_controlGame26_route(params) =>
      call { 
        controllers_AdminController_controlGame26_invoker.call(AdminController_0.controlGame)
      }
  
    // @LINE:39
    case controllers_AdminController_incHomeGoal27_route(params) =>
      call { 
        controllers_AdminController_incHomeGoal27_invoker.call(AdminController_0.incHomeGoal)
      }
  
    // @LINE:44
    case controllers_security_LoginController_login28_route(params) =>
      call { 
        controllers_security_LoginController_login28_invoker.call(LoginController_5.login)
      }
  
    // @LINE:45
    case controllers_security_LoginController_loginSubmit29_route(params) =>
      call { 
        controllers_security_LoginController_loginSubmit29_invoker.call(LoginController_5.loginSubmit)
      }
  
    // @LINE:46
    case controllers_security_LoginController_logout30_route(params) =>
      call { 
        controllers_security_LoginController_logout30_invoker.call(LoginController_5.logout)
      }
  
    // @LINE:52
    case controllers_CountController_count31_route(params) =>
      call { 
        controllers_CountController_count31_invoker.call(CountController_1.count)
      }
  
    // @LINE:54
    case controllers_AsyncController_message32_route(params) =>
      call { 
        controllers_AsyncController_message32_invoker.call(AsyncController_3.message)
      }
  
    // @LINE:57
    case controllers_Assets_versioned33_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned33_invoker.call(Assets_4.versioned(path, file))
      }
  }
}
