
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/wdd/webapps/ca_gaa/conf/routes
// @DATE:Mon Apr 24 07:17:30 IST 2017

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers.javascript {
  import ReverseRouteContext.empty

  // @LINE:57
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:57
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:15
  class ReverseAdminController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:27
    def deleteTeam: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.deleteTeam",
      """
        function(tid0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/delTeam/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("tid", tid0)})
        }
      """
    )
  
    // @LINE:16
    def fixture: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.fixture",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/fixture"})
        }
      """
    )
  
    // @LINE:20
    def referee: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.referee",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/referee"})
        }
      """
    )
  
    // @LINE:25
    def addTeam: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.addTeam",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/addTeam"})
        }
      """
    )
  
    // @LINE:23
    def deletePlayer: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.deletePlayer",
      """
        function(pid0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "delPlayer/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("pid", pid0)})
        }
      """
    )
  
    // @LINE:15
    def adminIndex: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.adminIndex",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/"})
        }
      """
    )
  
    // @LINE:17
    def league: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.league",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/league"})
        }
      """
    )
  
    // @LINE:35
    def deleteReferee: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.deleteReferee",
      """
        function(rid0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/delReferee/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("rid", rid0)})
        }
      """
    )
  
    // @LINE:30
    def addFixtureSubmit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.addFixtureSubmit",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/addFixtureSubmit"})
        }
      """
    )
  
    // @LINE:32
    def updateFixture: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.updateFixture",
      """
        function(fid0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/updateFixture/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("fid", fid0)})
        }
      """
    )
  
    // @LINE:29
    def addFixture: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.addFixture",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/addFixture"})
        }
      """
    )
  
    // @LINE:28
    def updateTeam: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.updateTeam",
      """
        function(tid0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/updateTeam/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("tid", tid0)})
        }
      """
    )
  
    // @LINE:34
    def addRefereeSubmit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.addRefereeSubmit",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/addRefereeSubmit"})
        }
      """
    )
  
    // @LINE:21
    def addPlayer: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.addPlayer",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/addPlayer"})
        }
      """
    )
  
    // @LINE:39
    def incHomeGoal: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.incHomeGoal",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/incHomeGoal/"})
        }
      """
    )
  
    // @LINE:18
    def player: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.player",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/player"})
        }
      """
    )
  
    // @LINE:19
    def team: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.team",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/team"})
        }
      """
    )
  
    // @LINE:33
    def addReferee: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.addReferee",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/addReferee"})
        }
      """
    )
  
    // @LINE:38
    def controlGame: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.controlGame",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/controlGame"})
        }
      """
    )
  
    // @LINE:24
    def updatePlayer: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.updatePlayer",
      """
        function(pid0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/updatePlayer/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("pid", pid0)})
        }
      """
    )
  
    // @LINE:31
    def deleteFixture: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.deleteFixture",
      """
        function(fid0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/delFixture/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("fid", fid0)})
        }
      """
    )
  
    // @LINE:22
    def addPlayerSubmit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.addPlayerSubmit",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/addPlayerSubmit"})
        }
      """
    )
  
    // @LINE:26
    def addTeamSubmit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AdminController.addTeamSubmit",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/addTeamSubmit"})
        }
      """
    )
  
  }

  // @LINE:52
  class ReverseCountController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:52
    def count: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.CountController.count",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "count"})
        }
      """
    )
  
  }

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:9
    def fixtures: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.fixtures",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "fixtures"})
        }
      """
    )
  
    // @LINE:7
    def live: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.live",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "live"})
        }
      """
    )
  
    // @LINE:8
    def results: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.results",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "results"})
        }
      """
    )
  
    // @LINE:6
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
    // @LINE:10
    def table: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.table",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "table"})
        }
      """
    )
  
  }

  // @LINE:54
  class ReverseAsyncController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:54
    def message: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AsyncController.message",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "message"})
        }
      """
    )
  
  }


}
